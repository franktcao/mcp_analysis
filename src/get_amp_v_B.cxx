#include "Run_Settings.cxx"
#include "Run_Results.cxx"
#include "../src/get_results.cxx"

void get_amp_v_B( ){
  
  Run_Settings settings("run_conditions.txt");
  Run_Results  results;

  std::vector<int> allruns = settings.GetRunList();
  
  //Get all runs with 1X amplifier
  std::vector<int> amp1_spe_runs   = settings.GetRunsWithAmplification(1);
  std::vector<int> amp10_spe_runs  = settings.GetRunsWithAmplification(10);
  std::vector<int> amp100_spe_runs = settings.GetRunsWithAmplification(100);
  
  //Get all runs in SPE Mode (from the previous run list)
  amp1_spe_runs   = settings.GetRunsWithMode("SPE", amp1_spe_runs);
  amp10_spe_runs  = settings.GetRunsWithMode("SPE", amp10_spe_runs);
  amp100_spe_runs = settings.GetRunsWithMode("SPE", amp100_spe_runs);
  
  //std::vector<int> relevant_runs = amp1_spe_runs;
  std::vector<int> relevant_runs = amp10_spe_runs;
  //std::vector<int> relevant_runs = amp100_spe_runs;
  
  //settings.PrintRows( relevant_runs );
  //return;

  // Sort run list by high voltage (HV) and extract unique HVs
  settings.SortRunListByHV( relevant_runs );
  std::vector<float> hv_datasets = settings.GetUniqueHVs( relevant_runs );
  
  std::vector<float> Bs;
  std::vector<float> amplitudes, d_amplitudes;
  
  //amp100_spe_runs = {1006, 2006};
  
  int icol = 0;
  std::vector<int> good_colors = {1, 4, 2, 6, 8, 9, 11, 28, 38, 46, 49};
  TMultiGraph * mgraph = new TMultiGraph("mg", "Amplitude vs. B; B [kG]; Amplitude [mV]");
  TGraphErrors * graph = nullptr;
  TCanvas * c1 = new TCanvas("c1","c1",600,400);
  for( auto hv : hv_datasets ){
    std::vector<int> hv_runs = settings.GetRunsWithHV( hv, relevant_runs );
    Bs.clear();
    amplitudes.clear();
    int n_runs = hv_runs.size();
    for( auto run : hv_runs ){
      settings.SetValues( run );

      float B = settings.GetBField();
      Bs.push_back( B );

      results = get_results( run );
      double amp = results.GetAmplitude();
      double d_amp = results.GetAmplitudeWidth();

      amplitudes.push_back( amp );
      d_amplitudes.push_back( d_amp );
      cout << endl;
      cout << "================================================" << endl;
      cout << "Run " << run << " : " << amp << " +/- " << d_amp << endl;
      cout << "================================================" << endl;
      cout << endl;
    }
    std::vector<float> zeros( d_amplitudes.size(), 0 );
    graph = new TGraphErrors( n_runs, Bs.data(), amplitudes.data(), zeros.data(), d_amplitudes.data() );
    graph->SetTitle(Form("%.f [V]", hv));
    graph->SetMarkerStyle(8);
    graph->SetDrawOption("AP");
    graph->SetMarkerColor(good_colors.at(icol));
    graph->SetLineColor(good_colors.at(icol));
    
    mgraph->Add(graph);
    icol++;
  }

  
  c1 = new TCanvas("c1","c1",600,400);
  mgraph->Draw("AP");
  c1->BuildLegend();
  c1->Print("images/ampVB_10X_SPE.png");
  c1->Print("images/pdf/ampVB_10X_SPE.pdf");
  
  for( auto hv : hv_datasets ){
    std::vector<int> hv_runs = settings.GetRunsWithHV( hv, relevant_runs );
    settings.PrintRows(hv_runs); 
    cout << endl;
    cout << endl;
  }


return;

}


