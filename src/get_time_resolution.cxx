#include "Run_Settings.cxx"
#include "Run_Results.cxx"
#include "../src/get_results.cxx"

void get_time_resolution( int amp = 10, std::string mode = "SPE" ){
  
  // Set run conditions from table 
  Run_Settings settings("run_conditions.txt");
  Run_Results  results;

  std::vector<int> allruns = settings.GetRunList();
  
  //Get all runs with 1X, 10X, or 100X amplifier setting
  std::vector<int> relevant_runs = settings.GetRunsWithAmplification(amp);
  
  //Get all runs in SPE Mode (from the previous run list)
  relevant_runs = settings.GetRunsWithMode(mode, relevant_runs);
  
  //std::vector<int> relevant_runs = {1006};
  
  settings.PrintRows( relevant_runs );
  //return;

  // Sort run list by high voltage (HV) and extract unique HVs
  settings.SortRunListByHV( relevant_runs );
  std::vector<float> hv_datasets = settings.GetUniqueHVs( relevant_runs );
  
  
  // Vector of vector of results [hv_setting][run_value]
  std::vector<std::vector<double>> V_amps;
  std::vector<std::vector<double>> V_d_amps;
  std::vector<std::vector<double>> V_gains;
  std::vector<std::vector<double>> V_delta_ts;
  std::vector<std::vector<double>> V_d_delta_ts;
  std::vector<std::vector<double>> V_delta_xs;
  
  
  //amp100_spe_runs = {1006, 2006};
  
  std::string output_dir = to_string(amp) + "x_" + mode;
  int icol = 0;
  std::vector<int> good_colors = {1, 4, 2, 6, 8, 9, 11, 28, 38, 46, 49};
  TMultiGraph * mgraph1 = new TMultiGraph("mg1", Form("Amplifier = %iX, Mode = %s; B [kG]; #Delta t [ns]", amp, mode.c_str()) );
  TMultiGraph * mgraph2 = new TMultiGraph("mg2", Form("Amplifier = %iX, Mode = %s; B [kG]; #Amp     [mV]", amp, mode.c_str()) );
  TMultiGraph * mgraph3 = new TMultiGraph("mg3", Form("Amplifier = %iX, Mode = %s; Pulse Height [mV]; #Delta t [ns]; ", amp, mode.c_str()) );
  //TMultiGraph * mgraph4 = new TMultiGraph("mg4", Form("Amplifier = %iX, Mode = %s; B [kG]; #Amp     [mV]", amp, mode.c_str()) );
  TGraphErrors * graph = nullptr;
  TGraphErrors * graph2= nullptr;
  TGraphErrors * graph3= nullptr;
  TCanvas * c1 = new TCanvas("c1","c1",600,400);
  // Loop over high voltage settings
  for( auto hv : hv_datasets ){
    std::vector<int> hv_runs = settings.GetRunsWithHV( hv, relevant_runs );
    std::vector<double> Bs;
    std::vector<double> amps;
    std::vector<double> gains;
    std::vector<double> delta_ts;
    std::vector<double> delta_xs;
    std::vector<double> d_amps;
    std::vector<double> d_gains;
    std::vector<double> d_delta_ts;
    std::vector<double> d_delta_xs;
    int n_runs = hv_runs.size();
    // Loop over runs with that high voltage settings
    for( auto run : hv_runs ){
      settings.SetValues( run );

      float B = settings.GetBField();

      results = get_results( run, "images/" + output_dir );
      
      double amp   = results.GetAmplitude();
      double d_amp = results.GetAmplitudeWidth();
      double gain  = results.GetGain();
      double dt    = results.GetTimingResolution();
      double dx    = results.GetPositionResolution();
      double d_dt  = results.GetTimingResolutionWidth();

      if( dt < 1 ){
        Bs.push_back( B );
        amps.push_back( amp );
        d_amps.push_back( d_amp );
        gains.push_back( gain );
        delta_ts.push_back( dt );
        d_delta_ts.push_back( d_dt );
        
        delta_xs.push_back( dx );
      }
      cout << "\n================================================\n";
      cout << "Run " << run << " : " << dt <<  "\n";
      cout << "================================================\n\n";
    }
    std::vector<double> zeros( Bs.size(), 0 );
    graph = new TGraphErrors( Bs.size(), Bs.data(), delta_ts.data(), zeros.data(), d_delta_ts.data() );
    graph->SetTitle(Form("%.f [V]", hv));
    graph->SetMarkerStyle(8);
    graph->SetDrawOption("AP");
    graph->SetMarkerColor(good_colors.at(icol));
    graph->SetLineColor(good_colors.at(icol));
    
    mgraph1->Add(graph);
    
    graph2 = new TGraphErrors( Bs.size(), Bs.data(), amps.data(), zeros.data(), d_amps.data() );
    graph2->SetTitle(Form("%.f [V]", hv));
    graph2->SetMarkerStyle(8);
    graph2->SetDrawOption("AP");
    graph2->SetMarkerColor(good_colors.at(icol));
    graph2->SetLineColor(good_colors.at(icol));
    
    mgraph2->Add(graph2);
    
    graph3 = new TGraphErrors( amps.size(), amps.data(), delta_ts.data(), d_amps.data(), d_delta_ts.data() );
    graph3->SetTitle(Form("%.f [V]", hv));
    graph3->SetMarkerStyle(8);
    graph3->SetDrawOption("AP");
    graph3->SetMarkerColor(good_colors.at(icol));
    graph3->SetLineColor(good_colors.at(icol));
    
    mgraph3->Add(graph3);

    V_amps.push_back( amps );
    V_d_amps.push_back( d_amps );
    V_gains.push_back( gains );
    V_delta_ts.push_back( delta_ts );
    V_d_delta_ts.push_back( d_delta_ts );
    V_delta_xs.push_back( delta_xs );

    icol++;
  }
  
  
  c1 = new TCanvas("c1","c1",600,400);
  c1->SetLogx();
  mgraph1->Draw("AP");
  c1->BuildLegend();
  c1->Print(Form("images/dtVB_%iX_%s.png", amp, mode.c_str() ));
  c1->Print(Form("images/pdf/dtVB_%iX_%s.pdf", amp, mode.c_str() ));
  
  mgraph2->Draw("AP");
  c1->BuildLegend();
  c1->Print(Form("images/ampVB_%iX_%s.png", amp, mode.c_str() ));
  c1->Print(Form("images/pdf/ampVB_%iX_%s.pdf", amp, mode.c_str() ));
  
  mgraph3->Draw("AP");
  c1->BuildLegend();
  c1->Print(Form("images/dtVamp_%iX_%s.png", amp, mode.c_str() ));
  c1->Print(Form("images/pdf/dtVamp_%iX_%s.pdf", amp, mode.c_str() ));
  
  int ihv = 0;
  ofstream summary_table( Form("out/txt/run_summary_%iX_%s.txt", amp, mode.c_str() ) );
  std::string header = settings.PrintHeader( false );
  header += "\t Pulse Height [mV] \t delta Pulse Height [mV] \t Gain [ ] \t Time Resolution [ns] \t delta Time Resolution [ns] \t Position Resolution [mm]";
  
  summary_table << header << "\n";
  for( auto hv : hv_datasets ){
    std::vector<int> hv_runs = settings.GetRunsWithHV( hv, relevant_runs );
    std::vector<std::string> table = settings.PrintRows(hv_runs, false ); 
    std::vector<double> amps    = V_amps    .at( ihv );
    std::vector<double> d_amps  = V_d_amps  .at( ihv );
    std::vector<double> gains   = V_gains   .at( ihv );
    std::vector<double> dts     = V_delta_ts.at( ihv );
    std::vector<double> d_dts   = V_d_delta_ts.at( ihv );
    std::vector<double> dxs     = V_delta_xs.at( ihv );
    for( int irow = 1; irow < table.size(); irow++ ){
      std::string &row = table .at(irow);
      double amp       = amps  .at( irow-1 );
      double d_amp     = d_amps.at( irow-1 );
      double gain      = gains .at( irow-1 );
      double dt        = dts   .at( irow-1 );
      double d_dt      = d_dts .at( irow-1 );
      double dx        = dxs   .at( irow-1 );
      
      char result_row_buf[500];
      sprintf( result_row_buf, "\t %2.3f \t %2.3f \t %.3e \t %.3e \t %.3e \t %.3e ", amp, d_amp, gain,  dt, d_dt,  dx );
      std::string result_row = (string) result_row_buf;
      row += result_row;
      summary_table << row << "\n";
    }

    ihv++;

  }
  summary_table.close();


return;

}


