#include "individual_run_root_analyzer.cxx"
#include "MCP_Event.cxx"
#include "loop_events.h"

void loop_events( int runBegin, int runEnd ){
  if( runEnd < runBegin ) runEnd = runBegin;  
  //auto allstats = getSTLfromTXT( "data/18_Summer/run_settings.txt" );
  auto allstats = getSTLfromTXT( "data/18_Winter/run_settings.txt" );
  auto stats = allstats[0];
  for( Int_t irun = runBegin; irun < runEnd + 1; irun++ ){

    stats.assign( allstats[0].size(), 0 );
    for( auto s : allstats ){ if( irun == s[0] ){ stats = s; break; } }
    if( stats[0] == 0 ){ cout << "No such run number setting " << endl; continue; }
    
    // Get File
    TChain * inTree  = new TChain("lappd");
    TString fname = "rootfiles/rootfile_run_" + std::to_string(irun) + ".root"; 
    inTree->Add(fname);
    // Set up new files/trees to write to 
    Int_t nevents = inTree->GetEntries();
    if( nevents < 0 ){ cout << "Get Outta Here " << endl; continue;}

    TString ssfileName =  "out/MCP_run_" + to_string(irun) + ".root";
    TFile* skimFile = new TFile(ssfileName, "recreate");
    MCP_Event * event = new MCP_Event;
    TTree *MCPTree = new TTree("simpletree", "A simple tree"); 
    TBranch *MCPBranch = MCPTree->Branch("MCP_event", "MCP_Event", &event);
    // Add all of the relevant files   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    // IO   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 

    // New Class
    individual_run_root_analyzer runData( inTree );
    // Get Settings
    // Loop over events

    double &run    = stats[0];
    double &B      = stats[1];
    double &phi    = stats[2];
    double &theta  = stats[3];
    double &V_MCP  = stats[4];
    double &V_LED  = stats[5];
    double &dt_LED = stats[6];
    double &V_PC   = stats[7];
    
    event = new MCP_Event( run, B, phi, theta, V_MCP );
    event->InitOtherSettings( V_PC, V_LED, dt_LED );

    initHists();
    //std::vector<double> sig;
    //std::vector<double> t_ns;
    for(Long64_t ievent = 0; ievent< nevents; ievent++){
      runData.GetEntry(ievent);
      //cout << runData.evtno << endl;

      Float_t  * fwav_gain       = runData.fwav_gain; 
      Float_t  * fwav_amp        = runData.fwav_amp; 
      Double_t * fwav_time       = runData.fwav_time; 
      Float_t * fwav_peaktime = runData.fwav_peaktime; 
      Double_t * fwav_risingtime = runData.fwav_risingtime; 
      Double_t * fwav_fallingtime = runData.fwav_fallingtime; 
      //vector<float> fwav_vol = runData.fwav_vol[5];

//cout << fwav_vol.size() << endl;
//for( auto b : fwav_vol ){ cout << b << " aaa ";}
//cout << endl;

      //if( fwav_time[1] < 0 ) continue;
      //if( fwav_time[2] < 0 ) continue;
      //if( fwav_amp [1] < 0 ) continue;
      //if( fwav_amp [2] < 0 ) continue;

      double gain   = (double) fwav_gain[5];
      
      //for( int ii = 0; ii < 10; ii++ ){
      //  cout << fwav_peaktime[ii] << " \t" ;
      //  //cout << fwav_risingtime[ii] << " \t" ;
      //  //cout << fwav_fallingtime[ii] << " \t" ;
      //  //cout << fwav_gain[ii] << " \t" ;
      //  //cout << fwav_amp[ii] << " \t" ;
      //  //cout << fwav_time[ii] << " \t" ;
      //}
      //cout << endl;
      
      //double amp    = (double) (fwav_amp[1]+fwav_amp[2])/2.0;
      double amp    = (double) (fwav_amp[5])/1.0;
      //double t_sig  = (double) (fwav_time[1] + fwav_time[2])/2.0;
      //double t_trig = (double) fwav_time[0];
      double t_sig  = (double) fwav_peaktime[5];
      double t_trig = (double) fwav_peaktime[0];
      //cout << t_sig << endl;

      h_Gains  -> Fill( gain           );
      h_amp    -> Fill( amp            );
      //cout << t_sig * 0.2 << endl;
      //cout << t_sig - t_trig << endl;
      h_timing -> Fill( t_sig - t_trig );
      h_pos_res-> Fill( t_sig * 0.2    );   // 0.2 is the time bin size
      
      //vector<float> t           = runData.t; 
      //vector<float> *vol_fft    = runData.fwav_vol_fft; 

      //  if( runData.eventno == 1 ){
      //std::vector<double> sig     .push_back( vol_fft[1] );
      //std::vector<double> t_ns    .push_back( t/1000 );
      //}

    }

    // Set Results 
    TCanvas * c = new TCanvas;
    std::vector<double> vals;
    getFitValues( *h_Gains, vals );
    if( vals.size() > 0 ){
      event->gain = vals[1];
      cout << "gain : " << event->gain << endl;
    }

    getFitValues( *h_amp, vals );
    if( vals.size() > 0 ){
      event->amp = vals[1];
      cout << "amp : " << event->amp << endl;
    }

    getFitValues( *h_timing, vals );
    if( vals.size() > 0 ){
      event->dt = vals[2];
      cout << "dt : " << event->dt << endl;
    }

    getFitValues( *h_pos_res, vals );
    if( vals.size() > 0 ){
      event->dx = vals[2];
      cout << "dx : " << event->dx << endl;
    }
    //h_amp->Draw();
    h_timing->Draw();
    //h_pos_res->Draw();
    c->Print("Jidjdi.png");
    
    // Set Histograms
    event->h_gain   = *(  h_Gains   );
    event->h_amp    = *(  h_amp     );
    event->h_dt     = *(  h_timing  );
    event->h_dx     = *(  h_pos_res );

    //double ampmean = h_amp->GetMean();
    //double amprms  = h_amp->GetRMS();
    //double ampmax  = ampmean+5*amprms;

    //int n = inTree->Draw("evt.fwav[1].vol_fft:(evt.t/1000)","evt.evtno == 1"); 
    //cout << "n : " << n << endl;
    ////TGraph *gr0 =  (TGraph*)gPad->GetPrimitive("Graph");            
    ////gr0->Draw("al");
    ////gr0->SetTitle("Signal waveforms");
    ////gr0->GetXaxis()->SetTitle("t [ns]");
    ////gr0->GetXaxis()->SetRangeUser(-10,150);
    ////gr0->GetYaxis()->SetTitle("Output Voltage [mV]");
    ////gr0->GetYaxis()->SetRangeUser(-0.09*ampmax,.09*ampmax);
    ////cout << "ampmax " << ampmax << endl;
    ////gr0->Draw("al");
    ////event.h_signal = h_Gains ;//= TH1D("h_signal", "Signal Wave Form ; t [ns];",      100,  0,   110  );
    ////event->g_signal = *(  gr0       );

    MCPTree->Fill();
    skimFile->Write();

    delete event; // Most important
    delete MCPTree;
    delete h_Gains ;
    delete h_amp ;
    delete h_timing ;
    delete h_pos_res ;
    skimFile->Close();
    delete skimFile;
  }

}

void loop_events( int runBegin = 3009 ){
  loop_events( runBegin, runBegin );
}

///////////////////////// ///////////////////////// ///////////////////////// ///////////////////////// /////////////////////////

//______________________________________________________________________________________________________________________________________________

void getFitValues( TH1D &h, std::vector<double> &vals ){

  vals.clear();
  h.Fit("gaus");
  TF1* tf = (TF1*) h.GetListOfFunctions()->FindObject("gaus");
  if( tf == 0 ) return vals;
  Double_t amp   = tf->GetParameter(0);
  Double_t mean  = tf->GetParameter(1);
  Double_t width = abs(tf->GetParameter(2));
  vals.push_back( amp );
  vals.push_back( mean );
  vals.push_back( width );


}

//______________________________________________________________________________________________________________________________________________

void initHists(){

  h_Gains     = new TH1D("h_Gains","", 250, 100,40000000);   
  //h_amp       = new TH1D("h_amp","", 250, 0, 60);      
  h_amp       = new TH1D("h_amp","", 250, 0, 600);      
  //h_timing    = new TH1D("h_timing","", 250, 500,15000);      
  h_timing    = new TH1D("h_timing","", 250, 34000,35000);      
  //h_pos_res   = new TH1D("h_pos_res","", 250, -100,100);      
  //h_pos_res   = new TH1D("h_pos_res","", 250, -0.2*55000,0.2*55000);      
  //h_pos_res   = new TH1D("h_pos_res","", 250, 16000,19000);      
  h_pos_res   = new TH1D("h_pos_res","", 250, 15000,22000);      
  h_rise_time = new TH1D("h_rise_time","", 250, 145,165);      
  h_fall_time = new TH1D("h_fall_time","", 250, 145,165);      
}

//______________________________________________________________________________________________________________________________________________

std::vector<std::vector<double>> getSTLfromTXT( std::string s_in ){
  std::vector<std::vector<double>> result;
  string token;
  double val;
  string line;
  ifstream input( s_in );
  // Get line by line of file
  while(std::getline(input, line)){
    // Convert into a stream
    std::istringstream line_as_stream(line);
    std::vector<double> v_line;
    // Get each token of the line
    while(line_as_stream >> token){
      val = stod(token);
      v_line.push_back( val );
    }
    result.push_back( v_line );
  }
  return result;
}
