#include "MCP_Event.cxx"
#include "get_results.h"
#include "graphx/gfx.h"
#include "utilities/util.h"

void get_results( int runBegin = 3009, int runEnd = 3009 ){
  if( runEnd < runBegin ) runEnd = runBegin;  
  TChain * inTree  = new TChain("simpletree");
  for( Int_t irun = runBegin; irun < runEnd + 1; irun++ ){
    if( irun == 3028 || irun == 3029 || irun == 3030 ) continue;
    // Get File
    TString fname = "out/MCP_run_" + std::to_string(irun) + ".root"; 
    inTree->Add(fname);
  }

  // Set up new files/trees to write to 
  Int_t nevents = inTree->GetEntries();
  if( nevents < 0 ){ cout << "Get Outta Here " << endl; return;}

  MCP_Event * event = nullptr;

  // Add all of the relevant files   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

  // IO   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 

  inTree->SetBranchAddress("MCP_event",  &event);

  // Event Loop   ==========================================================================================
  
  std::vector<TString> g_titles;
  //// Select Study
  
  std::vector<std::vector<double>> phis, thetas, gains, amps, dts, dxs;
  std::vector<float> V_MCPs = { 100, 200, 300 };
  std::vector<float> Bs     = { 0.18, 0.7 };
  //std::vector<float> study = V_MCPs;
  //double &study_val = event->V_MCP;
  //for( int ii = 0; ii < study.size(); ii++ ){ g_titles.push_back( "B = 0.7 kG, #theta = 0, V = " + TString( format_digi(study[ii], 0) ) + " V" ); }
  //std::vector<std::vector<double>> &main_x = phis; 
  //TString x_name = "phi";
  //for( int ii = 0; ii < study.size(); ii++ ){ 
  //  for( int jj = 0; jj < study[ii].size(); jj++ ){ 
  //    g_titles.push_back( "B = " + format_digi( study[ii][jj], 2 ) + "kG, #theta = 0, V = " + format_digi( study[ii][jj], 0 ) + " V" ); 
  //  }
  //}
  std::vector<float> study = Bs;
  double &study_val = event->B;
  for( int ii = 0; ii < study.size(); ii++ ){ 
    g_titles.push_back( "B = " + format_digi( study[ii], 2 ) + "kG, #theta = 0, V = " + format_digi( study[ii], 0 ) + " V" ); 
  }
  std::vector<std::vector<double>> &main_x = thetas;
  TString x_name = "theta";

  int n = study.size();
  phis  .resize(n); 
  thetas.resize(n);
  gains .resize(n);
  amps  .resize(n);
  dts   .resize(n);
  dxs   .resize(n);

  
  
  Long64_t nentries = inTree->GetEntries();
  for(Int_t ievent = 0; ievent< nevents; ievent++){
    inTree->GetEntry(ievent);
    double theta = event->theta;
    double phi   = event->phi;
    double V_MCP = event->V_MCP;
    
    double gain  = event->gain;
    double amp   = event->amp;
    double dt    = event->dt;
    double dx    = event->dx;

    // Selection Process
    int idx_B = 0;
    int idx_V = 0;
    auto it_B = std::find( Bs.begin(), Bs.end(), (float) Bs_val ); 
    if( it_B != Bs.end() ){
      idx = it_B - Bs.begin();
    }
    else{ cout << "something went wrong, B" << endl; continue; }
    
    auto it_V = std::find( V_MCPs.begin(), V_MCPs.end(), (float) V_MCPs_val ); 
    if( it_V != V_MCPs.end() ){
      idx = it_V - V_MCPs.begin();
    }
    else{ cout << "something went wrong, V" << endl; continue; }
    
    //if( amp > 50 ) continue;
    ////if( dt  > 0.1 ) continue;
    //if( gain  <= 0.0 ) continue;
    
    //cout << gain << endl;
    //cout << dt << endl;
    phis  [idx].push_back(phi);
    thetas[idx].push_back(theta);
    amps  [idx].push_back(amp);
    gains [idx].push_back(gain);
    dts   [idx].push_back(dt);
    dxs   [idx].push_back(dx);

  }
  TMultiGraph * mgraph = nullptr;
  
  mgraph = buildMGraph( main_x, amps, "ampV"+x_name, "Signal Amplitude vs. #" + x_name + " ; #" + x_name + " [deg.]; Signal Amplitude [mV];", g_titles );
  //drawMGraph( *mgraph, {10, 95, 0, 11} );
  drawMGraph( *mgraph );
  
  mgraph = buildMGraph( main_x, dts, "dtV"+x_name, "#Delta t vs. #" + x_name + "; #" + x_name + " [deg.]; #Delta t [ns];", g_titles );
  drawMGraph( *mgraph );
  
  mgraph = buildMGraph( main_x, gains, "gainV"+x_name, "Gain vs. #" + x_name + "; #" + x_name + " [deg.]; Gain [];", g_titles );
  drawMGraph( *mgraph, {}, true );
  
  mgraph = buildMGraph( main_x, dxs, "dxV"+x_name, "#Delta x vs. #" + x_name + "; #" + x_name + " [deg.]; #Delta x [mm];", g_titles );
  drawMGraph( *mgraph );

}

///////////////////////// ///////////////////////// ///////////////////////// ///////////////////////// /////////////////////////

TMultiGraph * buildMGraph( const std::vector<std::vector<double>> &x, const std::vector<std::vector<double>> &y, const TString &mg_name, const TString &mg_title, const std::vector<TString> &g_titles ){
  std::vector<EColor> colors = { kBlack, kBlue, kRed };
  TMultiGraph * mgraph = new TMultiGraph;
  mgraph->SetNameTitle( mg_name, mg_title );
  for( int ii = 0; ii < x.size(); ii++ ){
    TGraph * graph = new TGraph( x[ii].size(), x[ii].data(), y[ii].data() );
    graph->SetNameTitle( TString( ii ), g_titles[ii] );
    graph->SetMarkerColor( colors[ii] );
    graph->SetMarkerStyle(21);
    graph->SetMarkerSize(0.6);
    mgraph->Add( graph );
  }
  return mgraph;
}

///////////////////////// ///////////////////////// ///////////////////////// ///////////////////////// /////////////////////////

void drawMGraph( TMultiGraph & mg, std::vector<double> limits_xlo_yhi = {}, bool setlogy = false ){
  TCanvas * c1 = new TCanvas;
  if( setlogy ) c1->SetLogy(); 
  mg.Draw("aps");
  mg.GetXaxis()->CenterTitle();
  mg.GetYaxis()->CenterTitle();
  if( limits_xlo_yhi.size() > 0 ){
    double xlo = limits_xlo_yhi.at(0);
    double xhi = limits_xlo_yhi.at(1);
    double ylo = limits_xlo_yhi.at(2);
    double yhi = limits_xlo_yhi.at(3);
    mg.GetXaxis()->SetLimits(xlo, xhi);
    mg.GetYaxis()->SetLimits(ylo, yhi);
  }
  c1->BuildLegend(0.1,0.7,0.5,0.9);
  printPNGandPDF( c1, mg.GetName(), "images/" );
}

///////////////////////// ///////////////////////// ///////////////////////// ///////////////////////// /////////////////////////

