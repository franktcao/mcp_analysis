#include "Run_Results.cxx"
#include "Run_Settings.cxx"
#include "individual_run_root_analyzer.cxx"
#include "get_results.h"
#include "graphx/gfx.h"
#include "utilities/util.h"

Run_Results get_results( int run = 1006, std::string output_dir = "0" ){
  Run_Results  results;
  Run_Settings settings("run_conditions.txt");

  // Get File
  TChain * inTree  = new TChain("lappd");
  TString fname = "rootfiles/rootfile_run_" + std::to_string(run) + ".root"; 
  inTree->Add(fname);
  
  // Set up new files/trees to write to 
  Int_t nevents = inTree->GetEntries();
  if( nevents < 0 ){ cout << "Get Outta Here " << endl; return results;}

  // New Class
  individual_run_root_analyzer runData( inTree );
  
  // Init Histograms
  initHists( run, settings );

  // Amplitudes to average over run
  std::vector<std::vector<double>> amps2d(10);
  std::vector<double> means(10, 0.);
  for(Long64_t ievent = 0; ievent < nevents; ievent++){
    runData.GetEntry(ievent);

    Float_t  * fwav_gain        = runData.fwav_gain; 
    Float_t  * fwav_amp         = runData.fwav_amp; 
    Double_t * fwav_time        = runData.fwav_time; 
    //Float_t * fwav_peaktime     = runData.fwav_peaktime; 
    //Double_t * fwav_risingtime  = runData.fwav_risingtime; 
    //Double_t * fwav_fallingtime = runData.fwav_fallingtime; 
     

    //cout << fwav_amp[5] << endl;

    //if( fwav_time[0]  <= 0 ) continue;
    ////if( fwav_time[1]  <= 0 ) continue;
    //if( fwav_time[2]  <= 0 ) continue;
    ////if( fwav_time[3]  <= 0 ) continue;
    //if( fwav_time[4]  <= 0 ) continue;
    //if( fwav_time[5]  <= 0 ) continue;
    //if( fwav_time[6]  <= 0 ) continue;
    ////if( fwav_time[7]  <= 0 ) continue;
    //if( fwav_time[8]  <= 0 ) continue;
    ////if( fwav_time[9]  <= 0 ) continue;
    ////if( fwav_amp[1]   <= 0 ) continue;
    //if( fwav_amp[2]   <= 0 ) continue;
    ////if( fwav_amp[3]   <= 0 ) continue;
    //if( fwav_amp[4]   <= 0 ) continue;
    //if( fwav_amp[5]   <= 0 ) continue;
    //if( fwav_amp[6]   <= 0 ) continue;
    ////if( fwav_amp[7]   <= 0 ) continue;
    //if( fwav_amp[8]   <= 0 ) continue;
    ////if( fwav_amp[9]   <= 0 ) continue;


    int i_sig = 5;                                  // Index of channel of interest (Center pixel: Index = 5)
    if( fwav_time[0]      < 0  ) continue;
    if( fwav_time[i_sig]  < 0  ) continue;
    if( fwav_time[i_sig]  < 2  ) continue;
    //if( fwav_amp [i_sig]  < 0  )  continue;
    //if( fwav_gain[i_sig] < 1E3 ) continue;

    float t_trig    = fwav_time[0];                 // Trigger time in [ps]

    float amp_sig   = fwav_amp [i_sig];             // Signal amplitude in [mV]
    float gain_sig  = fwav_gain[i_sig];             // Signal gain 
    
    float t_sig     = fwav_time[i_sig];             // Signal time in [ps]
    float t_sig_l   = fwav_time[i_sig-1];             // Signal time in [ps]
    float t_sig_r   = fwav_time[i_sig+1];             // Signal time in [ps]
    float t_sig_t   = fwav_time[i_sig+3];             // Signal time in [ps]
    float t_sig_b   = fwav_time[i_sig-3];             // Signal time in [ps]

    double delta_t   = abs( t_sig - t_trig  )/1000.;   // Time between signal and trigger in [ns]
    double delta_t_l = abs( t_sig - t_sig_l )/1000.;   // Time between signal and trigger in [ns]
    double delta_t_r = abs( t_sig - t_sig_r )/1000.;   // Time between signal and trigger in [ns]
    double delta_t_t = abs( t_sig - t_sig_t )/1000.;   // Time between signal and trigger in [ns]
    double delta_t_b = abs( t_sig - t_sig_b )/1000.;   // Time between signal and trigger in [ns]
    
    double dt = 0.1;                                // Time bin width
    double c = 300;                                 // speed of light in [mm/ns]
    double dx = 2./3. * c * delta_t_l  * dt;          // Position resolution: dx = 2/3 c Dt dt

    //if( dx < 1000 ) continue;
    h_amp    -> Fill( amp_sig );
    h_Gains  -> Fill( gain_sig );
    h_timing -> Fill( delta_t );
    h_pos_res-> Fill( dx );   

    //cout <<delta_t<<endl;
    //cout << amp_sig << endl;

    // accumulate signal 
    for( int ii = 0; ii < 10; ii++ ){ means.at(ii) += fwav_amp[ii]; }
  }
  for( auto m : means ){ m /= nevents; }

  //std::vector<double> amps;
  //for( auto amp_list : amps2d ){
  //  double acc = 0;
  //  for( auto a : amp_list ) acc += a; 
  //  double mean = acc / amp_list.size();
  //  amps.push_back( mean );
  //}

    

  h2_amp2d->Fill( 1,1, means.at(1) ); 
  h2_amp2d->Fill( 2,1, means.at(2) ); 
  h2_amp2d->Fill( 3,1, means.at(3) ); 
  h2_amp2d->Fill( 1,2, means.at(4) ); 
  h2_amp2d->Fill( 2,2, means.at(5) ); 
  h2_amp2d->Fill( 3,2, means.at(6) ); 
  h2_amp2d->Fill( 1,3, means.at(7) ); 
  h2_amp2d->Fill( 2,3, means.at(8) ); 
  h2_amp2d->Fill( 3,3, means.at(9) ); 


  // Set Results 
  double gain, amp, dt, dx = -99;
  double d_gain, d_amp, d_dt, d_dx = -99;
  
  std::vector<double> vals;
  getFitValues( h_Gains, vals );
  gain = vals[1];
  d_gain = vals[2];

  getFitValues( h_amp , vals );
  amp = vals[1];
  d_amp = vals[2];

  getFitValues_2gaus( h_timing, vals );
  //getFitValues_2gaus( TH1 * h, std::vector<double> &vals );
  dt = vals[2];
  d_dt = vals[3];

  getFitValues( h_pos_res, vals );
  dx = vals[2];
  
  gSystem->mkdir(Form("images/run_%d",run),true);
  gSystem->mkdir(Form("images/run_%d/pdf",run),true);
  
  TCanvas * c1 = new TCanvas;
  gStyle->SetOptFit();
  h_amp    ->Draw( );
  c1->Print( Form("images/run_%d/amplitudes_run_%d.png", run, run));
  c1->Print( Form("images/run_%d/pdf/amplitudes_run_%d.pdf", run, run));
  if( output_dir != "0" ){
    c1->Print( Form("%s/amplitudes_run_%d.png", output_dir.c_str(), run));
    c1->Print( Form("%s/pdf/amplitudes_run_%d.pdf", output_dir.c_str(), run));
  }
  h_Gains  ->Draw( );
  c1->Print( Form("images/run_%d/gains_run_%d.png", run, run));
  c1->Print( Form("images/run_%d/pdf/gains_run_%d.pdf", run, run));
  if( output_dir != "0" ){
    c1->Print( Form("%s/gains_run_%d.png", output_dir.c_str(), run));
    c1->Print( Form("%s/pdf/gains_run_%d.pdf", output_dir.c_str(), run));
  }
  h_timing ->Draw( );
  c1->Print( Form("images/run_%d/timing_run_%d.png", run, run));
  c1->Print( Form("images/run_%d/pdf/timing_run_%d.pdf", run, run));
  if( output_dir != "0" ){
    c1->Print( Form("%s/timing_run_%d.png", output_dir.c_str(), run));
    c1->Print( Form("%s/pdf/timing_run_%d.pdf", output_dir.c_str(), run));
  }
  h_pos_res->Draw( );   
  c1->Print( Form("images/run_%d/position_resolutions_run_%d.png", run, run));
  c1->Print( Form("images/run_%d/pdf/position_resolutions_run_%d.pdf", run, run));
  if( output_dir != "0" ){
    c1->Print( Form("%s/position_resolutions_run_%d.png", output_dir.c_str(), run));
    c1->Print( Form("%s/pdf/position_resolutions_run_%d.pdf", output_dir.c_str(), run));
  }
  h2_amp2d->Draw("lego1");
  c1->Print( Form("images/run_%d/amp2d_run_%d.png", run, run));
  c1->Print( Form("images/run_%d/pdf/amp2d_run_%d.pdf", run, run));
  if( output_dir != "0" ){
    c1->Print( Form("%s/amp2_run_%d.png", output_dir.c_str(), run));
    c1->Print( Form("%s/pdf/amp2_run_%d.pdf", output_dir.c_str(), run));
  }


  results.SetResults( amp, gain, dt, dx, d_amp, d_gain, d_dt );

  delete h_Gains ;
  delete h_amp ;
  delete h_timing ;
  delete h_pos_res ;
  
  return results;

  //int n = inTree->Draw("evt.fwav[1].vol_fft:(evt.t/1000)","evt.evtno == 1"); 
  //cout << "n : " << n << endl;
  ////TGraph *gr0 =  (TGraph*)gPad->GetPrimitive("Graph");            
  ////gr0->Draw("al");
  ////gr0->SetTitle("Signal waveforms");
  ////gr0->GetXaxis()->SetTitle("t [ns]");
  ////gr0->GetXaxis()->SetRangeUser(-10,150);
  ////gr0->GetYaxis()->SetTitle("Output Voltage [mV]");
  ////gr0->GetYaxis()->SetRangeUser(-0.09*ampmax,.09*ampmax);
  ////cout << "ampmax " << ampmax << endl;
  ////gr0->Draw("al");
  ////event.h_signal = h_Gains ;//= TH1D("h_signal", "Signal Wave Form ; t [ns];",      100,  0,   110  );
  ////event->g_signal = *(  gr0       );

}


///////////////////////// ///////////////////////// ///////////////////////// ///////////////////////// /////////////////////////

//______________________________________________________________________________________________________________________________________________

void getFitValues_2gaus( TH1 * h, std::vector<double> &vals ){
  vals.clear(); 
  
  Double_t min, max = 0;
  min = h->GetBinCenter( h->FindFirstBinAbove(1) );
  max = h->GetBinCenter( h->FindLastBinAbove(1) ); 
  double mid = .5 * ( max + min );
  double dist = .5 * ( max - min );
  cout << min << "\t\t " << mid << "\t\t" << max << endl;
  TF1 *tf = new TF1("dgaus","gaus(0) + gaus(3)", min, max);
  //TF1 *tf = new TF1("dgaus","gaus(0)+ [3] + [4]*x + [5]*x*x + [6]*x*x*x + [7]*x*x*x*x", min, max);
  //TF1 *tf = new TF1("dgaus","[0] + [1]*x + [2]*x*x + [3]*x*x*x + [4]*x*x*x*x + gaus(5) ", min, max);
  //TF1* tf = (TF1*) h->GetListOfFunctions()->FindObject("gaus+gaus");

  //tf->SetParameter(0, 30);
  //tf->SetParameter(1, max );
  //tf->SetParameter(1, h->GetBinCenter(mid) );
  tf->SetParameter(0, h->GetMaximum());
  tf->SetParameter(1, h->GetMean() );
  tf->SetParameter(2, h->GetRMS()/10 );
  
  tf->SetParameter(3, h->GetMaximum()/2);
  tf->SetParameter(4, h->GetMean() );
  tf->SetParameter(5, h->GetRMS());

  h->Fit("dgaus");
  if( tf == 0 ) return vals;
  Double_t amp1    = tf->GetParameter(0);
  Double_t mean1   = tf->GetParameter(1);
  Double_t width1  = abs(tf->GetParameter(2));
  Double_t dwidth1 = abs(tf->GetParError(2));

  Double_t amp2    = tf->GetParameter(3);
  Double_t mean2   = tf->GetParameter(4);
  Double_t width2  = abs(tf->GetParameter(5));
  Double_t dwidth2 = abs(tf->GetParError(5));

  Double_t chi2  = tf->GetChisquare();
  if( amp2 > amp1 ){
    amp1   = amp2;
    mean1  = mean2;
    width1 = width2;
    dwidth1 = dwidth2;
  }
  if( dwidth1 > 0.5 * width1 ) dwidth1 = 0.5 *width1;
  if( dwidth1 > h->GetRMS()  ) dwidth1 = h->GetRMS();
  if( dwidth1 > 1            ) dwidth1 = 1;
    
  if( chi2 > 50 ){
    amp1   = h->GetMaximum();
    mean1  = h->GetMean();
    width1 = h->GetRMS();
  }
  cout << "CHI^2 " << chi2 << endl;
  cout << "MEAN " << mean1 << " " << mean2 << endl;
  cout << "WIDTH " << width1 << " " << width2 << endl;
  
  vals.push_back( amp1 );
  vals.push_back( mean1 );
  vals.push_back( width1 );
  vals.push_back( dwidth1  );
  return vals;

}

//______________________________________________________________________________________________________________________________________________

void getFitValues( TH1 * h, std::vector<double> &vals ){
  vals.clear(); 
  
  h->Fit("gaus");
  TF1* tf = (TF1*) h->GetListOfFunctions()->FindObject("gaus");
  if( tf == 0 ) return vals;
  Double_t amp   = tf->GetParameter(0);
  Double_t mean  = tf->GetParameter(1);
  Double_t width = abs(tf->GetParameter(2));
  Double_t chi2  = tf->GetChisquare();
  if( chi2 > 50 ){
    amp   = h->GetMaximum();
    mean  = h->GetMean();
    width = h->GetRMS();
  }
  cout << "CHI^2 " << chi2 << endl;
  cout << "MEAN " << mean << endl;
  cout << "WIDTH " << width << endl;
  
  vals.push_back( amp );
  vals.push_back( mean );
  vals.push_back( width );
  return vals;

}

//______________________________________________________________________________________________________________________________________________

void initHists( int run, Run_Settings &settings ){
  float       hv   = settings.GetHV(run);
  float       B    = settings.GetBField(run);
  int         amp  = settings.GetAmplifier(run); 
  std::string mode = settings.GetMode( run );

  h_Gains     = new TH1F(Form("h_Gains_%i", run),   Form("HV = %.f, B = %.f kG, Amplifier = %iX, Mode = %s (Run %i); Gain [ ]; Counts", hv, B, amp, mode.c_str(), run), 250, 1E2,   1E6);   
  h_amp       = new TH1F(Form("h_amp_%i", run),     Form("HV = %.f, B = %.f kG, Amplifier = %iX, Mode = %s (Run %i); Amplitude [mV]; Counts", hv, B, amp, mode.c_str(), run), 250, 0, 1);      
  h_timing    = new TH1F(Form("h_timing_%i", run),  Form("HV = %.f, B = %.f kG, Amplifier = %iX, Mode = %s (Run %i); #Delta t [ns]; Counts", hv, B, amp, mode.c_str(), run), 120, 33.7, 35.2    );      
  h_pos_res   = new TH1F(Form("h_pos_res_%i", run), Form("HV = %.f, B = %.f kG, Amplifier = %iX, Mode = %s (Run %i); #Delta x [mm]; Counts", hv, B, amp, mode.c_str(), run), 250, 0, 1    );      
  h2_amp2d    = new TH2I(Form("h_amp2d_%i",run),    Form("HV = %.f, B = %.f kG, Amplifier = %iX, Mode = %s (Run %i); Row; Column; Amplitude [mV];", hv, B, amp, mode.c_str(), run), 3, 1, 3, 3, 1, 3);

  h_Gains   ->SetCanExtend(TH1::kAllAxes);
  h_amp     ->SetCanExtend(TH1::kAllAxes);
  //h_timing  ->SetCanExtend(TH1::kAllAxes);
  h_pos_res ->SetCanExtend(TH1::kAllAxes);

}

//______________________________________________________________________________________________________________________________________________

