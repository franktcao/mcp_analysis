 // ---------------------------------------------------------\\
 //                                                          \\
 //                 Bin2TxtConv_CAEN.cc                      \\
 //                                                          \\
 // Written by: Mohammad Hattawy (mohammad.hattawy@gmail.com)\\
 // Date      : July 3rd, 2017                               \\
 //                                                          \\
 // Function: - reads CAEN binary files                      \\
 //           - applies the offset to the waveforms          \\
 //           - applies ADCTomV calibrations                 \\
 //           - and write them in txt format                 \\
 // requirments: root and gcc                                \\
 // compile:root -b -q "Bin2TxtConv_CAEN.cc(\"data_dir\")"   \\
 //----------------------------------------------------------\\

 
 #include <iostream> 
 #include <iomanip> 
 #include <fstream>
 #include <string> 

 using namespace std;

 // function to find the run number from the data directory--------------
 int Find_runNumber(const char* DIRNAME)
 {
    int RunNumber= 0;
    int fullNameLength = strlen(DIRNAME);
    string StringRunNumber;
    int  FNP = fullNameLength - 4;   // first number position
 
    for (int ii = FNP; ii<FNP+5; ii++)  StringRunNumber += DIRNAME[ii];
 
    std::istringstream iss(StringRunNumber);
    iss >> RunNumber;
    cout<<" run number  "<<RunNumber<<endl;
   return RunNumber;
  } 

 // the main reading and writing scripts --------------------------------- 
 void Bin2TxtConv_CAEN_9Ch(const char* DIRNAME = "data") 
 {
     // Calibration converstion to mV --------------------- 
     const float cnts2mv= (40.0/150.0) /1000.00; // lappd mutiply by 1000.00, we divide by it to set it to one 

     // input files ----------------------------------------
     const int Nevt = 10000;   // number of events to be read from each file 
     const int Nch = 10;      // number of channels to be read

     // trigger of the MCP is [0] --------------------------
     // left side of the MCP is [1] ------------------------
     // right side of the MCP is [2] -----------------------

     ifstream infile[Nch]; 
     char *inname[Nch]; 
     inname[0] = Form("%s/TR_0_0.dat",DIRNAME); 
     inname[1] = Form("%s/wave_0.dat",DIRNAME); 
     inname[2] = Form("%s/wave_1.dat",DIRNAME); 
     inname[3] = Form("%s/wave_2.dat",DIRNAME); 
     inname[4] = Form("%s/wave_3.dat",DIRNAME); 
     inname[5] = Form("%s/wave_4.dat",DIRNAME); 
     inname[6] = Form("%s/wave_5.dat",DIRNAME); 
     inname[7] = Form("%s/wave_6.dat",DIRNAME); 
     inname[8] = Form("%s/wave_7.dat",DIRNAME); 
     inname[9] = Form("%s/wave_8.dat",DIRNAME); 
     float XX[Nch]; 

     for(int ii=0; ii<Nch; ii++) {
        cout<<inname[ii]<<endl;
        infile[ii].open(inname[ii], ios::binary); 
        if(!infile[ii]){ cout << "There was a problem reading file " << inname[ii]<< endl; }
     } 


     // output txt file ------------------------------------------------
     const int runNumber = Find_runNumber(DIRNAME);
     ofstream outfile;
     outfile.open(Form("txtfiles/txtfile_run_%d.txt",runNumber));

     // averaging the signals to reduce the noise 
     const int NR = 1;

     // loop over the events from the three input files ---------------
     for( int jj=0; jj<Nevt; jj=jj+NR){
        // write the title of the event in the output file ------------
        outfile<<"#trig Evt "<<jj
        <<"\t "<<"bot left "<<jj
        <<"\t "<<"bot ctr "<<jj
        <<"\t "<<"bot right "<<jj
        <<"\t "<<"mid left "<<jj
        <<"\t "<<"mid ctr "<<jj
        <<"\t "<<"mid right "<<jj
        <<"\t "<<"top left "<<jj
        <<"\t "<<"top ctr "<<jj
        <<"\t "<<"top right "<<jj
        <<endl;  
        
        // define the arrays to save individual data bins
        const int n_bins = 1024;
        vector<float> bins_TR_0(n_bins);
        vector<float> bins_Wave_0(n_bins);
        vector<float> bins_Wave_1(n_bins);
        vector<float> bins_Wave_2(n_bins);
        vector<float> bins_Wave_3(n_bins);
        vector<float> bins_Wave_4(n_bins);
        vector<float> bins_Wave_5(n_bins);
        vector<float> bins_Wave_6(n_bins);
        vector<float> bins_Wave_7(n_bins);
        vector<float> bins_Wave_8(n_bins);
        
        for(int ii=0; ii<NR; ii++){       // sum over NR waveforms  
           for(int kk=0; kk<n_bins; kk++){ 

              infile[0].read((char *)&XX[0], sizeof(float)); 
              infile[1].read((char *)&XX[1], sizeof(float)); 
              infile[2].read((char *)&XX[2], sizeof(float)); 
              infile[3].read((char *)&XX[3], sizeof(float)); 
              infile[4].read((char *)&XX[4], sizeof(float)); 
              infile[5].read((char *)&XX[5], sizeof(float)); 
              infile[6].read((char *)&XX[6], sizeof(float)); 
              infile[7].read((char *)&XX[7], sizeof(float)); 
              infile[8].read((char *)&XX[8], sizeof(float)); 
              infile[9].read((char *)&XX[9], sizeof(float)); 
           
              bins_TR_0[kk]   += XX[0]; 
              bins_Wave_0[kk] += XX[1];
              bins_Wave_1[kk] += XX[2];
              bins_Wave_2[kk] += XX[3];
              bins_Wave_3[kk] += XX[4];
              bins_Wave_4[kk] += XX[5];
              bins_Wave_5[kk] += XX[6];
              bins_Wave_6[kk] += XX[7];
              bins_Wave_7[kk] += XX[8];
              bins_Wave_8[kk] += XX[9];
           
           //if(jj%1020 == 0 ) cout<<" Event # "<< jj<<"  bin "<<kk<<"    "
           //                 << bins_TR_1[kk]<<"     "<<fixed <<setprecision(6)<<XX[0]<<endl;
          }
        }

        // now average the total time bins of NR waveforms ------------- 
        for(int kk=0; kk<n_bins; kk++){
            
            bins_TR_0[kk]   = (float) bins_TR_0[kk]  /NR; 
            bins_Wave_0[kk] = (float) bins_Wave_0[kk]/NR;
            bins_Wave_1[kk] = (float) bins_Wave_1[kk]/NR;
            bins_Wave_2[kk] = (float) bins_Wave_2[kk]/NR;
            bins_Wave_3[kk] = (float) bins_Wave_3[kk]/NR;
            bins_Wave_4[kk] = (float) bins_Wave_4[kk]/NR;
            bins_Wave_5[kk] = (float) bins_Wave_5[kk]/NR;
            bins_Wave_6[kk] = (float) bins_Wave_6[kk]/NR;
            bins_Wave_7[kk] = (float) bins_Wave_7[kk]/NR;
            bins_Wave_8[kk] = (float) bins_Wave_8[kk]/NR;
         }

        // find the offset from the first 128 timebins in each channel --------

        const int noise_bins = 128;

        float sum_offset_TR_0   = 0.0;
        float sum_offset_Wave_0 = 0.0;
        float sum_offset_Wave_1 = 0.0;
        float sum_offset_Wave_2 = 0.0; 
        float sum_offset_Wave_3 = 0.0; 
        float sum_offset_Wave_4 = 0.0; 
        float sum_offset_Wave_5 = 0.0; 
        float sum_offset_Wave_6 = 0.0; 
        float sum_offset_Wave_7 = 0.0; 
        float sum_offset_Wave_8 = 0.0; 

        float mean_offset_TR_0   = 0.0;
        float mean_offset_Wave_0 = 0.0;
        float mean_offset_Wave_1 = 0.0;
        float mean_offset_Wave_2 = 0.0;
        float mean_offset_Wave_3 = 0.0;
        float mean_offset_Wave_4 = 0.0;
        float mean_offset_Wave_5 = 0.0;
        float mean_offset_Wave_6 = 0.0;
        float mean_offset_Wave_7 = 0.0;
        float mean_offset_Wave_8 = 0.0;
 
        for(int kk=0; kk<noise_bins; kk++){
           sum_offset_TR_0   += bins_TR_0[kk];
           sum_offset_Wave_0 += bins_Wave_0[kk];
           sum_offset_Wave_1 += bins_Wave_1[kk];
           sum_offset_Wave_2 += bins_Wave_2[kk];
           sum_offset_Wave_3 += bins_Wave_3[kk];
           sum_offset_Wave_4 += bins_Wave_4[kk];
           sum_offset_Wave_5 += bins_Wave_5[kk];
           sum_offset_Wave_6 += bins_Wave_6[kk];
           sum_offset_Wave_7 += bins_Wave_7[kk];
           sum_offset_Wave_8 += bins_Wave_8[kk];
        }

        mean_offset_TR_0   = sum_offset_TR_0  /128.0;
        mean_offset_Wave_0 = sum_offset_Wave_0/128.0;
        mean_offset_Wave_1 = sum_offset_Wave_1/128.0;
        mean_offset_Wave_2 = sum_offset_Wave_2/128.0;
        mean_offset_Wave_3 = sum_offset_Wave_3/128.0;
        mean_offset_Wave_4 = sum_offset_Wave_4/128.0;
        mean_offset_Wave_5 = sum_offset_Wave_5/128.0;
        mean_offset_Wave_6 = sum_offset_Wave_6/128.0;
        mean_offset_Wave_7 = sum_offset_Wave_7/128.0;
        mean_offset_Wave_8 = sum_offset_Wave_8/128.0;
   
        // Apply correction/calibration to waveform data and -------
        // write after bin 23 -----------------------

        for(int mm=0; mm<n_bins; mm++){
           
           bins_TR_0[mm]   = (bins_TR_0[mm]   - mean_offset_TR_0)*cnts2mv;
           bins_Wave_0[mm] = (bins_Wave_0[mm] - mean_offset_Wave_0)*cnts2mv;
           bins_Wave_1[mm] = (bins_Wave_1[mm] - mean_offset_Wave_1)*cnts2mv;
           bins_Wave_2[mm] = (bins_Wave_2[mm] - mean_offset_Wave_2)*cnts2mv;
           bins_Wave_3[mm] = (bins_Wave_3[mm] - mean_offset_Wave_3)*cnts2mv;
           bins_Wave_4[mm] = (bins_Wave_4[mm] - mean_offset_Wave_4)*cnts2mv;
           bins_Wave_5[mm] = (bins_Wave_5[mm] - mean_offset_Wave_5)*cnts2mv;
           bins_Wave_6[mm] = (bins_Wave_6[mm] - mean_offset_Wave_6)*cnts2mv;
           bins_Wave_7[mm] = (bins_Wave_7[mm] - mean_offset_Wave_7)*cnts2mv;
           bins_Wave_8[mm] = (bins_Wave_8[mm] - mean_offset_Wave_8)*cnts2mv;
           
           if( mm > 23 )
           outfile<<bins_TR_0[mm]
           <<"\t "<<bins_Wave_0[mm]
           <<"\t "<<bins_Wave_1[mm]
           <<"\t "<<bins_Wave_2[mm]
           <<"\t "<<bins_Wave_3[mm]
           <<"\t "<<bins_Wave_4[mm]
           <<"\t "<<bins_Wave_5[mm]
           <<"\t "<<bins_Wave_6[mm]
           <<"\t "<<bins_Wave_7[mm]
           <<"\t "<<bins_Wave_8[mm]
           <<endl;  

        }

     } // end the loop on Nevt



     // closing the inut and the output data files --------------------
     for(int ii=0; ii<Nch; ii++) infile[ii].close();
     outfile.close();

 }  // endl the main function 
