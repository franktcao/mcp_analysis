#ifndef util_h
#define util_h
#include <iostream>
#include <string>
#include <sstream>
//______________________________________________________________________________

void printProgress( int irun, int nstart, int nend, std::string name = "Run Number" ){
  // Prints the progress 
  //cout << "\t " << name << ": \t" << (Float_t) irun << " \t / \t " << nend << " \t (";
  //cout << (Float_t) (irun- nstart) / (nend - nstart)*100 <<  " %)" << endl;
  // modified from https://ideone.com/Yg8NKj

  float progress = (float) (irun- nstart) / (nend - nstart);
  const int barWidth = 70;
  std::cout << "\t\t[";
  int pos = barWidth * progress;
  for( int i = 0; i < barWidth; ++i ){
    if (i < pos) std::cout << "=";
    else if (i == pos) std::cout << ">";
    else std::cout << " ";
  }
  std::cout << "] " << int(progress*100) << " % " << " ( " << irun << " / " << nend <<" events ) \r";
  std::cout.flush();
  if( irun == nend -1 )std::cout << std::endl;
}

//______________________________________________________________________________

void printProgress_2( Int_t irun, Int_t nstart, Int_t nend, string name = "Run Number" ){
  // Prints the progress 
    //if( ievent % (Int_t) 1E5 == 0 ){
    //  cout << "\t Run Number: " << runNum ;
    //  cout << "\t Event: \t" << (Float_t) ievent/ (Int_t) 1E3 << " \t k  / \t ";
    //  cout << (Float_t) nentries  / (Int_t) 1E3 << " k   \t=\t ";
    //  cout << (Float_t) ievent / (Int_t) (nentries)*100 <<  "\t %" << endl;
    //}
  cout << "\t " << name << ": \t" << (Float_t) irun << " \t / \t " << nend << " \t (";
  cout << (Float_t) (irun- nstart) / (nend - nstart)*100 <<  " %)" << endl;
}

//______________________________________________________________________________

string format_digi( Double_t number, Int_t digits = 3 ){
  // Returns a string of number with specified digits
  stringstream ss;
  ss << fixed << setprecision(digits) << number;
  return ss.str();
}

//______________________________________________________________________________

void logBinning(TH1* h){

   TAxis *axis = h->GetXaxis();
   int bins = axis->GetNbins();

   Double_t from      = axis->GetXmin();
   Double_t to        = axis->GetXmax();
   Double_t width     = (to - from) / bins;
   cout << to << " " << from << " " << width << endl;
   std::vector<Double_t> new_bins(bins + 1);

   new_bins[0] = from;
   for (int i = 1; i <= bins; i++) {
     int idx = bins - i + 1;
     double new_width = TMath::Power(1/10., i-1);
     new_bins[idx] = new_width;
     cout << new_bins[idx] << " ";
   }
   cout << endl;
   axis->Set(bins, new_bins.data());
   //delete new_bins;
}
//_________________________________________________________________________________________________________

void appendHistNameTitle( TH1 &hist, string n, string t="" ){
  // Append n and t to the histogram's name and title, respectively
  string name = (string)  hist.GetName();
  string title = (string) hist.GetTitle();

  hist.SetName( (name+n).c_str() );
  hist.SetTitle( (title+t).c_str() );
}


//_________________________________________________________________________________________________________

void appendHist2NameTitle( TH2 &hist, string n, string t="" ){
  // Append n and t to the histogram's name and title, respectively
  string name = (string)  hist.GetName();
  string title = (string) hist.GetTitle();

  hist.SetName( (name+n).c_str() );
  hist.SetTitle( (title+t).c_str() );
}

//______________________________________________________________________________

void printline( std::vector<Float_t> list, string delim = " " ){
  // Prints std::vector into line
  for( auto ii : list ){ cout << ii << delim; }
  cout << endl; 
}

//______________________________________________________________________________

void printRule(string ruleChar = "=", int length = 120){

  for( int ii = 0; ii < length; ii++){
    std::cout << ruleChar;
  }
  std::cout << std::endl;

}

//______________________________________________________________________________________________________________________________________________

double getProgressPct(double current, double start, double end){

  Double_t num = current - start;
  Double_t den = end + 1 - start;
  return num / den * 100;

}

//______________________________________________________________________________________________________________________________________________
  

std::vector<std::vector<double>> getSTLfromTXT( std::string s_in ){
  std::vector<std::vector<double>> result;
  string token;
  double val;
  string line;
  ifstream input( s_in );
  // Get line by line of file
  while(std::getline(input, line)){
    // Convert into a stream
    std::istringstream line_as_stream(line);
    std::vector<double> v_line;
    // Get each token of the line
    while(line_as_stream >> token){
      val = stod(token);
      v_line.push_back( val );
    }
    result.push_back( v_line );
  }
  return result;
}

//______________________________________________________________________________________________________________________________________________
#endif 
