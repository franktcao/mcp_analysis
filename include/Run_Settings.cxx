#ifndef Run_Settings_cxx
#define Run_Settings_cxx

#include "Run_Settings.h"


//////////////////////////////////////////////////////////////////////////

Run_Settings::Run_Settings( const std::string &_fn ){ LoadInDataTable( _fn ); }

int         Run_Settings::GetRunNumber(){          return run_num;               }
int         Run_Settings::GetNumberOfEvents(){     return n_events;              }
float       Run_Settings::GetHV(){                 return HV;                    }
float       Run_Settings::GetBField(){             return B;                     }
float       Run_Settings::GetTheta(){              return theta;                 }
float       Run_Settings::GetPhi(){                return phi;                   }
int         Run_Settings::GetAmplifier(){          return amplifier;             }
std::string Run_Settings::GetMode(){               return mode;                  }

int         Run_Settings::GetNumberOfEvents( int _runnum ){ return n_events_list.at(GetRunIndex(_runnum)); }
float       Run_Settings::GetHV(        int _runnum ){ return HV_list.at(GetRunIndex(_runnum)); }
float       Run_Settings::GetBField(    int _runnum ){ return B_list.at(GetRunIndex(_runnum)); }
float       Run_Settings::GetTheta(     int _runnum ){ return theta_list.at(GetRunIndex(_runnum)); }
float       Run_Settings::GetPhi(       int _runnum ){ return phi_list.at(GetRunIndex(_runnum)); }
int         Run_Settings::GetAmplifier( int _runnum ){ return amplifier_list.at(GetRunIndex(_runnum)); }
std::string Run_Settings::GetMode( int _runnum ){      return mode_list.at(GetRunIndex(_runnum)); }

int                      Run_Settings::GetRun( int _irun ){     return runnum_list.at(_irun); }
int                      Run_Settings::GetRunIndex( int _run ){ return run_to_index.at(_run); }

std::vector<int>         Run_Settings::GetRunList(){            return runnum_list;           }
std::vector<float>       Run_Settings::GetHVList(){             return HV_list;               }
std::vector<float>       Run_Settings::GetBList(){              return B_list;                }
std::vector<float>       Run_Settings::GetThetaList(){          return theta_list;            }
std::vector<float>       Run_Settings::GetPhiList(){            return phi_list;              }
std::vector<int>         Run_Settings::GetAmplifierList(){      return amplifier_list;        }
std::vector<std::string> Run_Settings::GetModeList(){           return mode_list;             }

std::vector<std::string> Run_Settings::Print(bool display = true){                 return PrintRows( runnum_list, display );     }

//////////////////////////////////////////////////////////////////////////

template <typename T>
void Run_Settings::GetVectorOfUniques( std::vector<T> &sorted_list ){
  // Returns vector of unique values (Requires input to be sorted!)
  
  // Get unique iterators
  //std::vector<T>::iterator it;
  auto it = std::unique(sorted_list.begin(), sorted_list.end());   

  // Get rid of bad iterators
  sorted_list.resize( std::distance(sorted_list.begin(),it) ); 
  
}

//////////////////////////////////////////////////////////////////////////

std::vector<float> Run_Settings::GetUniqueHVs( std::vector<int> _run_subset ){
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  
  std::vector<int> runs_sorted_by_hv = SortRunListByHV( _run_subset );
  
  // Get unique voltages
  std::vector<float> hvs = GetHVList( _run_subset );
  GetVectorOfUniques( hvs );

  return hvs;
}

//////////////////////////////////////////////////////////////////////////

std::vector<float> Run_Settings::GetUniqueAmplifications( std::vector<int> _run_subset ){
  std::vector<float> result;
  return result;
  //if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  //
  //std::vector<int> runs_sorted_by_hv = SortRunListByAmpification( _run_subset );
  //
  //// Get unique voltages
  //std::vector<float> hvs = GetHVList( _run_subset );
  //GetVectorOfUniques( hvs );

  //return hvs;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::GetRunsWithMode( std::string _mode, std::vector<int> _run_subset ){
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  std::vector<int> run_list;
  for( int ii = 0; ii < _run_subset.size(); ii++ ){
    int run = _run_subset.at(ii);
    int irun = GetRunIndex( run );
    if( mode_list.at(irun) == _mode ){ run_list.push_back( run ); }
  }
  return run_list;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::GetRunsWithB( int _B, std::vector<int> _run_subset ){
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  std::vector<int> run_list;
  for( int ii = 0; ii < _run_subset.size(); ii++ ){
    int run = _run_subset.at(ii);
    int irun = GetRunIndex( run );
    if( B_list.at(irun) == _B ){ run_list.push_back( run ); }
  }
  return run_list;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::GetRunsWithHV( float _HV, std::vector<int> _run_subset ){
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  std::vector<int> run_list;
  for( int ii = 0; ii < _run_subset.size(); ii++ ){
    int run = _run_subset.at(ii);
    int irun = GetRunIndex( run );
    if( HV_list.at(irun) == _HV ){ run_list.push_back( run ); }
  }
  return run_list;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::GetRunsWithPhi( int _phi, std::vector<int> _run_subset ){
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  std::vector<int> run_list;
  for( int ii = 0; ii < _run_subset.size(); ii++ ){
    int run = _run_subset.at(ii);
    int irun = GetRunIndex( run );
    if( phi_list.at(irun) == _phi ){ run_list.push_back( run ); }
  }
  return run_list;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::GetRunsWithTheta( int _theta, std::vector<int> _run_subset ){
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  std::vector<int> run_list;
  for( int ii = 0; ii < _run_subset.size(); ii++ ){
    int run = _run_subset.at(ii);
    int irun = GetRunIndex( run );
    if( theta_list.at(irun) == _theta ){ run_list.push_back( run ); }
  }
  return run_list;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::GetRunsWithAmplification( int _amp, std::vector<int> _run_subset ){
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;
  std::vector<int> run_list;
  for( int ii = 0; ii < _run_subset.size(); ii++ ){
    int run = _run_subset.at(ii);
    int irun = GetRunIndex( run );
    if( amplifier_list.at(irun) == _amp ){ run_list.push_back( run ); }
  }
  return run_list;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::SortRunListByVar( std::string &var, std::vector<int> &_run_subset ){ 
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;


  auto get_var  = [&]( int run ){ 
    if(      var == "HV" )        return GetHV(run);
    else if( var == "Amplifier" ) return (float) GetAmplifier(run);
    else if( var == "BField" )    return GetBField(run);
    else if( var == "Theta" )     return GetHV(run);
    else if( var == "Phi" )       return GetPhi(run);
    return (float) -999.;
  };
  
  auto comparer = [&]( int run1, int run2 ){ return get_var(run1) < get_var(run2); };
  std::sort( _run_subset.begin(), _run_subset.end(), comparer );
  return _run_subset;           
}

//////////////////////////////////////////////////////////////////////////

std::vector<int> Run_Settings::SortRunListByHV( std::vector<int> &_run_subset ){ 
  if( _run_subset.size() < 1 ) _run_subset = runnum_list;

  auto hv_lessthan = [&]( int run1, int run2 ){ return GetHV(run1) < GetHV(run2); };
  std::sort( _run_subset.begin(), _run_subset.end(), hv_lessthan );
  return _run_subset;           
}

//////////////////////////////////////////////////////////////////////////

std::vector<float>         Run_Settings::GetHVList( const std::vector<int> &_run_subset ){
  std::vector<float> result;
  for( auto r : _run_subset ) result.push_back( GetHV(r) );
  return result;
}

//////////////////////////////////////////////////////////////////////////

std::vector<float>       Run_Settings::GetBList( const std::vector<int> &_run_subset ){
  std::vector<float> result;
  for( auto r : _run_subset ) result.push_back( GetBField(r) );
  return result;
}

//////////////////////////////////////////////////////////////////////////

std::vector<float>       Run_Settings::GetThetaList( const std::vector<int> &_run_subset ){
  std::vector<float> result;
  for( auto r : _run_subset ) result.push_back( GetTheta(r) );
  return result;
}

//////////////////////////////////////////////////////////////////////////

std::vector<float>       Run_Settings::GetPhiList( const std::vector<int> &_run_subset ){
  std::vector<float> result;
  for( auto r : _run_subset ) result.push_back( GetPhi(r) );
  return result;
}

//////////////////////////////////////////////////////////////////////////

std::vector<int>         Run_Settings::GetAmplifierList( const std::vector<int> &_run_subset ){
  std::vector<int> result;
  for( auto r : _run_subset ) result.push_back( GetAmplifier(r) );
  return result;
}

//////////////////////////////////////////////////////////////////////////

std::vector<std::string> Run_Settings::GetModeList( const std::vector<int> &_run_subset ){
  std::vector<std::string> result;
  for( auto r : _run_subset ) result.push_back( GetMode(r) );
  return result;
}

//////////////////////////////////////////////////////////////////////////

std::string Run_Settings::PrintHeader( bool display = true ){

  std::string header = "Run_num \tN_events \tHV [V] \tB [T] \ttheta [deg.] \tphi [deg.] \tAmplifier \tMode=MPE,SPE";
  if( display ) std::cout << header << std::endl;
  return header;

}

//////////////////////////////////////////////////////////////////////////

std::vector<std::string> Run_Settings::PrintRows( const std::vector<int> &_runlist, bool display = true ){
  
  std::vector<std::string> table;

  int run_holder = run_num;
  std::string header = PrintHeader( display );
  
  table.push_back(header);
  
  for( int ii = 0; ii < _runlist.size(); ii++ ){
    int run = _runlist.at(ii);
    SetValues( run );
    char row_buf [500];
    //std::string row = to_string(run_num)    + "\t\t" +  to_string(n_events)  +"\t\t" +  to_string(HV)        +"\t" +  to_string(B)         +"\t"    +  to_string(theta)     +"\t\t"   +  to_string(phi)       +"\t\t"    +  to_string(amplifier) +"\t\t"  +  mode;
    sprintf( row_buf, "%i \t\t%i \t\t%.f \t%.f \t%.f \t\t%.f \t\t%i \t\t%s", run_num, n_events,  HV,  B, theta, phi,  amplifier,  mode.c_str() );
    std::string row = (string) row_buf;
    if( display ) std::cout << row <<  std::endl;
    table.push_back( row );
      
  }
  SetValues( run_holder );
  return table;

}

//////////////////////////////////////////////////////////////////////////

std::string Run_Settings::PrintRow( bool display = true ){

    char row_buf [500];
    //std::string row = to_string(run_num)    + "\t\t" +  to_string(n_events)  +"\t\t" +  to_string(HV)        +"\t" +  to_string(B)         +"\t"    +  to_string(theta)     +"\t\t"   +  to_string(phi)       +"\t\t"    +  to_string(amplifier) +"\t\t"  +  mode;
    sprintf( row_buf, "%i \t\t%i \t\t%.f \t%.f \t%.f \t\t%.f \t\t%i \t\t%s", run_num, n_events,  HV,  B, theta, phi,  amplifier,  mode.c_str() );
    std::string row = (string) row_buf;
    if( display ) std::cout << row <<  std::endl;
  return row;

}

//////////////////////////////////////////////////////////////////////////

void Run_Settings::LoadInDataTable( const std::string &_fn ){
  
  string   line;
  ifstream input( _fn );

  // Get rid of the first header line
  string header_line;
  getline(input, header_line);

  // Get line by line of file
  while( std::getline(input, line) ){
    // Convert into a stream
    std::istringstream line_as_stream(line);
    string token;
    
    //Run_num     N_events     HV [V]      B [kG]     theta [deg.]    phi [deg.]  Amplifier    Mode=MPE,SPE
    line_as_stream >>  token;
    run_to_index.insert( std::make_pair( stoi(token), runnum_list.size()) );
    runnum_list.push_back( stoi(token) );
    
    line_as_stream >> token;
    n_events_list.push_back( stoi(token) );

    line_as_stream >>  token;
    HV_list.push_back( stof(token) );
    
    line_as_stream >>  token;
    B_list.push_back( stof(token) );
    
    line_as_stream >>  token;
    theta_list.push_back( stof(token) );
    
    line_as_stream >>  token;
    phi_list.push_back( stof(token) );
    
    line_as_stream >>  token;
    amplifier_list.push_back( stoi(token) );
    
    line_as_stream >>  token;
    mode_list.push_back( std::string(token) );
    
  }
  input.close();
  SetValues
  ( runnum_list.at(0) );
}

//////////////////////////////////////////////////////////////////////////
    
void Run_Settings::SetValues( int _runnum ){

    //int index = run_to_index[ _runnum ];
    int index = run_to_index.at( _runnum );

    run_num   = runnum_list   .at( index );   
    n_events  = n_events_list .at( index );
    HV        = HV_list       .at( index );   
    B         = B_list        .at( index );   
    theta     = theta_list    .at( index );  
    phi       = phi_list      .at( index );   
    amplifier = amplifier_list.at( index ); 
    mode      = mode_list     .at( index );   

}

//////////////////////////////////////////////////////////////////////////

#endif


//////////////////////////////////////////////////////////////////////////

//template <typename T>
//vector<int> sort_indexes(const vector<T> &v) {
//
//  // initialize original index locations
//  vector<int> idx(v.size());
//  iota(idx.begin(), idx.end(), 0);
//
//  // sort indexes based on comparing values in v
//  sort(idx.begin(), idx.end(),
//       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});
//
////for( auto i : idx ) cout << v.at(i) << endl;
//  return idx;
//}


//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

