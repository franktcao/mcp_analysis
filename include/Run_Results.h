#ifndef Run_Results_h
#define Run_Results_h

#include <iostream>
#include <string>

#include "TMath.h"

class Run_Results{

  public:

    double amplitude = -99;   // Signal Amplitude [mV]
    double gain      = -99;   // Gain []
    double dt        = -99;   // Timing Resolution
    double dx        = -99;   // Position Resolution   
    
    double d_amplitude = -99;   // Signal Amplitude width [mV]
    double d_gain      = -99;   // Gain width []
    double d_dt        = -99;   // Timing Resolution width
    double d_dx        = -99;   // Position Resolution width  


  public:
    virtual ~Run_Results(){};
    Run_Results(){};
    
    void SetAmplitude(double _amp);
    void SetGain(double _gain);
    void SetTimingResolution(double _dt);
    void SetPositionResolution(double _dx);
    void SetAmplitudeWidth(double _d_amp);
    void SetGainWidth(double _d_gain);
    void SetResults(double _amp, double _gain, double _dt, double _dx, double _d_amp = 0, double _d_gain = 0, double _d_dt = 0 , double _d_dx = 0);
    void SetTimingResolutionWidth(double _d_dt);
    void SetPositionResolutionWidth(double _d_dx);
    
    double GetAmplitude();
    double GetGain();
    double GetTimingResolution();
    double GetPositionResolution();
    
    double GetAmplitudeWidth();
    double GetGainWidth();
    double GetTimingResolutionWidth();
    double GetPositionResolutionWidth();
};

#endif

