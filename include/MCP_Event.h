#ifndef MCP_Event_h
#define MCP_Event_h

#include <iostream>
#include <string>

#include "TMath.h"

class MCP_Event {

  public:

    // Settings
    int    run_num ; // Run number
    double B       ; // Mag. Field                    [kG]
    double phi     ; // Azimuthal Angle               [deg.]
    double theta   ; // Polar Angle                   [deg.]
    double V_MCP   ; // High Voltage of MCP PMT       [V]
    double V_LED   ; // Voltage of LED                [V]
    double dt_LED  ; // Pulse Length of LED           [ns]
    double V_PC    ; // High Voltage of Photo Cathode [V]

    // Results
    double gain = 0; // Gain                          [  ]
    double amp  = 0; // Signal Amplitude              [mV]
    double dt   = 0; // Timing Resolution             [ps]
    double dx   = 0; // Position Resolution           [mm]
    
    // Histograms
    TGraph g_signal ;
    TH1D   h_gain   ;//= TH1D("h_gain",   "Gain [ ]; Gain [ ];",             100,  0,   1E6  );
    TH1D   h_amp    ;//= TH1D("h_amp",    "Amplitude [mV]; Amplitude [mV];", 100,  0,   60   );
    TH1D   h_dt     ;//= TH1D("h_dt",     "Timing [ps]; Timing [ps];",       100,  0,   15E3 );
    TH1D   h_dx     ;//= TH1D("h_dx",     "Position [mm]; Position [mm];",   100, -100, 100  );



  public:
    virtual ~MCP_Event(){};
    MCP_Event(){};
    MCP_Event( int rn, double _B, double _phi, double _theta, double _VMCP) : run_num(rn), B(_B), phi(_phi), theta(_theta), V_MCP(_VMCP){};
    void InitOtherSettings( double _VPC, double _VLED, double _dtLED );
    void SetResults( double _gain, double _amp, double _dt, double _dx );
    void SetHistograms( const TGraph &signal, const TH1D &gain, const TH1D &amp, const TH1D &dt, const TH1D &dx );
    //void SetHistograms( const TH1D &signal, const TH1D &gains, const TH1D &amp, const TH1D &dt, const TH1D &dx );
};



#endif
