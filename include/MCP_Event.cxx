#include "MCP_Event.h"

//MCP_Event( int rn, double _B, double _phi, double _theta, double _VMCP) : run_num(rn), B(_B), phi(_phi), theta(_theta), V_MCP(_VMCP){};

void MCP_Event::InitOtherSettings( double _VPC, double _VLED, double _dtLED ){
  V_PC = _VPC;
  V_LED = _VLED;
  dt_LED = _dtLED;
}

void MCP_Event::SetResults( double _gain, double _amp, double _dt, double _dx ){
  gain = _gain; 
  amp  = _amp ;
  dt   = _dt  ;
  dx   = _dx  ;
}

void MCP_Event::SetHistograms( const TGraph &signal, const TH1D &gain, const TH1D &amp, const TH1D &dt, const TH1D &dx ){
  g_signal = signal;
  h_gain   = gain  ;
  h_amp    = amp   ;
  h_dt     = dt    ;
  h_dx     = dx    ;
}
