#ifndef individual_run_root_analyzer_cxx
#define individual_run_root_analyzer_cxx
#include "individual_run_root_analyzer.h"

individual_run_root_analyzer::individual_run_root_analyzer(TTree *tree)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {

      // The following code should be used if you want this class to access
      // a single tree instead of a chain
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
      if (!f || !f->IsOpen()) {
         f = new TFile("Memory Directory");
      }
      f->GetObject("ch",tree);


      //// The following code should be used if you want this class to access a chain
      //// of trees.
      //TChain * chain = new TChain("ch","individual_run_root_analyzer");
      //chain->Add("rootfiles/rootfile_run_2005.root/lappd");
      //tree = chain;

   }
   Init(tree);
}

individual_run_root_analyzer::~individual_run_root_analyzer()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t individual_run_root_analyzer::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t individual_run_root_analyzer::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void individual_run_root_analyzer::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("fUniqueID", &fUniqueID, &b_evt_fUniqueID);
   fChain->SetBranchAddress("fBits", &fBits, &b_evt_fBits);
   fChain->SetBranchAddress("WavSampleRate", &WavSampleRate, &b_evt_WavSampleRate);
   fChain->SetBranchAddress("WavDimSize", &WavDimSize, &b_evt_WavDimSize);
   fChain->SetBranchAddress("WavDeltat", &WavDeltat, &b_evt_WavDeltat);
   fChain->SetBranchAddress("evtno", &evtno, &b_evt_evtno);
   fChain->SetBranchAddress("fwav", &fwav_, &b_evt_fwav_);
   fChain->SetBranchAddress("fwav.fUniqueID", fwav_fUniqueID, &b_fwav_fUniqueID);
   fChain->SetBranchAddress("fwav.fBits", fwav_fBits, &b_fwav_fBits);
   fChain->SetBranchAddress("fwav.DoFFT", fwav_DoFFT, &b_fwav_DoFFT);
   fChain->SetBranchAddress("fwav.ReflectionCancelling", fwav_ReflectionCancelling, &b_fwav_ReflectionCancelling);
   fChain->SetBranchAddress("fwav.Samplerate", fwav_Samplerate, &b_fwav_Samplerate);
   fChain->SetBranchAddress("fwav.Deltat", fwav_Deltat, &b_fwav_Deltat);
   fChain->SetBranchAddress("fwav.DimSize", fwav_DimSize, &b_fwav_DimSize);
   fChain->SetBranchAddress("fwav.baseline", fwav_baseline, &b_fwav_baseline);
   fChain->SetBranchAddress("fwav.noiserms", fwav_noiserms, &b_fwav_noiserms);
   fChain->SetBranchAddress("fwav.qfast", fwav_qfast, &b_fwav_qfast);
   fChain->SetBranchAddress("fwav.qtot", fwav_qtot, &b_fwav_qtot);
   fChain->SetBranchAddress("fwav.peaktime", fwav_peaktime, &b_fwav_peaktime);
   fChain->SetBranchAddress("fwav.gain", fwav_gain, &b_fwav_gain);
   fChain->SetBranchAddress("fwav.amp", fwav_amp, &b_fwav_amp);
   fChain->SetBranchAddress("fwav.time", fwav_time, &b_fwav_time);
   fChain->SetBranchAddress("fwav.risingtime", fwav_risingtime, &b_fwav_risingtime);
   fChain->SetBranchAddress("fwav.fallingtime", fwav_fallingtime, &b_fwav_fallingtime);
   fChain->SetBranchAddress("fwav.TotThreshold", fwav_TotThreshold, &b_fwav_TotThreshold);
   fChain->SetBranchAddress("fwav.MinimumTot", fwav_MinimumTot, &b_fwav_MinimumTot);
   fChain->SetBranchAddress("fwav.npeaks", fwav_npeaks, &b_fwav_npeaks);
   fChain->SetBranchAddress("fwav.evtno", fwav_evtno, &b_fwav_evtno);
   fChain->SetBranchAddress("fwav.chno", fwav_chno, &b_fwav_chno);
   fChain->SetBranchAddress("fwav.WavID", fwav_WavID, &b_fwav_WavID);
   fChain->SetBranchAddress("fwav.TriggerMarker", fwav_TriggerMarker, &b_fwav_TriggerMarker);
   fChain->SetBranchAddress("fwav.gmean", fwav_gmean, &b_fwav_gmean);
   fChain->SetBranchAddress("fwav.gsigma", fwav_gsigma, &b_fwav_gsigma);
   fChain->SetBranchAddress("fwav.gpeak", fwav_gpeak, &b_fwav_gpeak);
   fChain->SetBranchAddress("fwav.gtime", fwav_gtime, &b_fwav_gtime);
   fChain->SetBranchAddress("fwav.grisetime", fwav_grisetime, &b_fwav_grisetime);
   fChain->SetBranchAddress("fwav.gchi2", fwav_gchi2, &b_fwav_gchi2);
   fChain->SetBranchAddress("fwav.WavName[100]", fwav_WavName, &b_fwav_WavName);
   fChain->SetBranchAddress("fwav.SplineName[100]", fwav_SplineName, &b_fwav_SplineName);
   fChain->SetBranchAddress("fwav.SplineTitle[100]", fwav_SplineTitle, &b_fwav_SplineTitle);
   fChain->SetBranchAddress("fwav.PointsPerSpline", fwav_PointsPerSpline, &b_fwav_PointsPerSpline);
   fChain->SetBranchAddress("fwav.CFDName[100]", fwav_CFDName, &b_fwav_CFDName);
   fChain->SetBranchAddress("fwav.CFDTitle[100]", fwav_CFDTitle, &b_fwav_CFDTitle);
   fChain->SetBranchAddress("fwav.CFDSplineName[100]", fwav_CFDSplineName, &b_fwav_CFDSplineName);
   fChain->SetBranchAddress("fwav.CFDSplineTitle[100]", fwav_CFDSplineTitle, &b_fwav_CFDSplineTitle);
   fChain->SetBranchAddress("fwav.TimeStamp", fwav_TimeStamp, &b_fwav_TimeStamp);
   fChain->SetBranchAddress("fwav.vol", fwav_vol, &b_fwav_vol);
   fChain->SetBranchAddress("fwav.vol_cfd", fwav_vol_cfd, &b_fwav_vol_cfd);
   fChain->SetBranchAddress("fwav.vol_fft", &fwav_vol_fft, &b_fwav_vol_fft);
   fChain->SetBranchAddress("fwav.re_fft", fwav_re_fft, &b_fwav_re_fft);
   fChain->SetBranchAddress("fwav.im_fft", fwav_im_fft, &b_fwav_im_fft);
   fChain->SetBranchAddress("fwav.mag_fft", fwav_mag_fft, &b_fwav_mag_fft);
   fChain->SetBranchAddress("fwav.mag_processed", fwav_mag_processed, &b_fwav_mag_processed);
   fChain->SetBranchAddress("fwav.IfDynamicWindow", fwav_IfDynamicWindow, &b_fwav_IfDynamicWindow);
   fChain->SetBranchAddress("fwav.FitWindow_min", fwav_FitWindow_min, &b_fwav_FitWindow_min);
   fChain->SetBranchAddress("fwav.FitWindow_max", fwav_FitWindow_max, &b_fwav_FitWindow_max);
   fChain->SetBranchAddress("fwav.DisWindow_min", fwav_DisWindow_min, &b_fwav_DisWindow_min);
   fChain->SetBranchAddress("fwav.DisWindow_max", fwav_DisWindow_max, &b_fwav_DisWindow_max);
   fChain->SetBranchAddress("fwav.CutoffFrequency", fwav_CutoffFrequency, &b_fwav_CutoffFrequency);
   fChain->SetBranchAddress("fwav.ReflectionWindow_min", fwav_ReflectionWindow_min, &b_fwav_ReflectionWindow_min);
   fChain->SetBranchAddress("fwav.ReflectionWindow_max", fwav_ReflectionWindow_max, &b_fwav_ReflectionWindow_max);
   fChain->SetBranchAddress("fwav.AmpThreshold", fwav_AmpThreshold, &b_fwav_AmpThreshold);
   fChain->SetBranchAddress("fwav.Fraction_CFD", fwav_Fraction_CFD, &b_fwav_Fraction_CFD);
   fChain->SetBranchAddress("fwav.Delay_CFD", fwav_Delay_CFD, &b_fwav_Delay_CFD);
   fChain->SetBranchAddress("fwav.AnalysisOption", fwav_AnalysisOption, &b_fwav_AnalysisOption);
   fChain->SetBranchAddress("fnwav", &fnwav, &b_evt_fnwav);
   fChain->SetBranchAddress("ClusterSize", &ClusterSize, &b_evt_ClusterSize);
   fChain->SetBranchAddress("cutflag", &cutflag, &b_evt_cutflag);
   fChain->SetBranchAddress("t", &t, &b_evt_t);
   fChain->SetBranchAddress("fz", &fz, &b_evt_fz);
   fChain->SetBranchAddress("tabs", &tabs, &b_evt_tabs);
   fChain->SetBranchAddress("tdiff", &tdiff, &b_evt_tdiff);
   fChain->SetBranchAddress("x", &x, &b_evt_x);
   fChain->SetBranchAddress("y", &y, &b_evt_y);
   Notify();
}

Bool_t individual_run_root_analyzer::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void individual_run_root_analyzer::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t individual_run_root_analyzer::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
void individual_run_root_analyzer::Loop()
{
}
#endif // #ifdef individual_run_root_analyzer_cxx
