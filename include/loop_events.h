#ifndef loop_events_h
#define loop_events_h
void initHists();
//void getSTLfromTXT( std::string s_in );
std::vector<std::vector<double>> getSTLfromTXT( std::string s_in );
void getFitValues( TH1D &h, std::vector<double> &vals );

TH1D *h_Gains     = nullptr;
TH1D *h_amp       = nullptr;
TH1D *h_timing    = nullptr;
TH1D *h_pos_res   = nullptr;
TH1D *h_rise_time = nullptr;
TH1D *h_fall_time = nullptr;
#endif
