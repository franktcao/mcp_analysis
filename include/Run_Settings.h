#ifndef Run_Settings_h
#define Run_Settings_h

#include <iostream>
#include <string>

#include "TMath.h"

class Run_Settings{

  public:

    //Run_num     N_events     HV [V]      B [T]      theta [deg.]    phi [deg.]  Amplifier    Mode=MPE,SPE
    
    // Settings
    int         run_num   ;   // Run number
    int         n_events  ;   // Number of events
    float       HV        ;   // High Voltage of MCP PMT      [V]
    float       B         ;   // Mag. Field                   [kG]
    float       theta     ;   // Polar/tilt Angle             [deg.]
    float       phi       ;   // Azimuthal Angle              [deg.]
    int         amplifier ;   // Ampflier 1X, 10X, etc. 
    std::string mode      ;   // Single photoelectron mode "SPE" or Multiple photoelectron mode "MPE"
    
    // Populate these from data table
    std::vector<int>         runnum_list, n_events_list, amplifier_list;
    std::vector<float>       B_list, HV_list, theta_list, phi_list;
    std::vector<std::string> mode_list;
    
    
    // Maps run number to row number of data table/vector index
    std::map<int, int> run_to_index;


  public:
    virtual ~Run_Settings(){};
    Run_Settings(){};
    Run_Settings( const std::string &_filename );
    
    void             LoadInDataTable( const std::string &_filename );
    void             SetValues( int _runnum );
    std::string              PrintHeader( bool display = true );
    std::string              PrintRow( bool display = true );
    std::vector<std::string> PrintRows( const std::vector<int> &_runlist, bool display = true);
    std::vector<std::string> Print(bool display = true);
    int              GetRunNumber();
    int              GetNumberOfEvents();
    float            GetHV();
    float            GetBField();
    float            GetTheta();
    float            GetPhi();
    int              GetAmplifier();
    std::string      GetMode();
    
    int              GetRun( int _irun );
    int              GetRunIndex( int _run );
    std::vector<int> GetRunList();
    
    int              GetNumberOfEvents( int _runnum ); 
    float            GetHV( int _runnum );                 
    float            GetBField( int _runum );             
    float            GetTheta( int _runum );              
    float            GetPhi( int _runum );                
    int              GetAmplifier( int _runum );          
    std::string      GetMode( int _runum );               

    std::vector<float>       GetHVList();
    std::vector<float>       GetBList();
    std::vector<float>       GetThetaList();
    std::vector<float>       GetPhiList();
    std::vector<int>         GetAmplifierList();
    std::vector<std::string> GetModeList();
    
    std::vector<float>       GetHVList( const std::vector<int> &_run_subset );
    std::vector<float>       GetBList( const std::vector<int> &_run_subset );
    std::vector<float>       GetThetaList( const std::vector<int> &_run_subset );
    std::vector<float>       GetPhiList( const std::vector<int> &_run_subset );
    std::vector<int>         GetAmplifierList( const std::vector<int> &_run_subset );
    std::vector<std::string> GetModeList( const std::vector<int> &_run_subset );
    

    std::vector<int>         SortRunListByHV( std::vector<int> &_run_list_in );
    std::vector<int>         SortRunListByVar( std::string &var, std::vector<int> &_run_subset );
    std::vector<float>       GetUniqueHVs( std::vector<int> _run_subset );
    std::vector<float>       GetUniqueAmplifications( std::vector<int> _run_subset );
    //void GetVectorOfUniques( std::vector<float> &sorted_list );
    
    template <typename T>
      void GetVectorOfUniques( std::vector<T> &sorted_list );
    
    std::vector<int> GetRunsWithMode(          std::string _mode, std::vector<int> _run_subset = {} );
    std::vector<int> GetRunsWithPhi(           int _phi,          std::vector<int> _run_subset = {} );
    std::vector<int> GetRunsWithTheta(         int _theta,        std::vector<int> _run_subset = {} );
    std::vector<int> GetRunsWithAmplification( int _amp,          std::vector<int> _run_subset = {} );
    std::vector<int> GetRunsWithHV(            float _HV,         std::vector<int> _run_subset = {} );
    std::vector<int> GetRunsWithB(             int _B,            std::vector<int> _run_subset = {} );
    
};  

#endif


    //template <typename T>
    //  vector<int> sort_indexes(const vector<T> &v) ;
