#ifndef Run_Results_cxx
#define Run_Results_cxx

#include "Run_Results.h"


//////////////////////////////////////////////////////////////////////////
void Run_Results::SetAmplitude(double _amp){         amplitude = _amp; }
void Run_Results::SetGain(double _gain){             gain = _gain;     }
void Run_Results::SetTimingResolution(double _dt){   dt = _dt;         }
void Run_Results::SetPositionResolution(double _dx){ dx = _dx;         }

void Run_Results::SetAmplitudeWidth(double _d_amp){         d_amplitude = _d_amp; }
void Run_Results::SetGainWidth(double _d_gain){             d_gain = _d_gain;     }
void Run_Results::SetTimingResolutionWidth(double _d_dt){   d_dt = _d_dt;         }
void Run_Results::SetPositionResolutionWidth(double _d_dx){ d_dx = _d_dx;         }
//////////////////////////////////////////////////////////////////////////

//void Run_Results::SetResults(double _amp, double _gain, double _dt, double _dx, double _d_amp, double _d_gain, double _d_dt, double _d_dx){ 
void Run_Results::SetResults(double _amp, double _gain, double _dt, double _dx, double _d_amp, double _d_gain, double _d_dt, double _d_dx){ 
  SetAmplitude(_amp)        ;
  SetGain(_gain)            ;
  SetTimingResolution(_dt)  ;
  SetPositionResolution(_dx);
  
  if( _d_amp != 0  ) SetAmplitudeWidth(_d_amp) ;
  if( _d_gain != 0 ) SetGainWidth(_d_gain)            ;
  if( _d_dt != 0   ) SetTimingResolutionWidth(_d_dt)  ;
  if( _d_dx != 0   ) SetPositionResolutionWidth(_d_dx);
}


double Run_Results::GetAmplitude()         { return amplitude; }
double Run_Results::GetGain()              { return gain;      }
double Run_Results::GetTimingResolution()  { return dt;        }
double Run_Results::GetPositionResolution(){ return dx;        }

double Run_Results::GetAmplitudeWidth()         { return d_amplitude; }
double Run_Results::GetGainWidth()              { return d_gain;      }
double Run_Results::GetTimingResolutionWidth()  { return d_dt;        }
double Run_Results::GetPositionResolutionWidth(){ return d_dx;        }

//////////////////////////////////////////////////////////////////////////

#endif
