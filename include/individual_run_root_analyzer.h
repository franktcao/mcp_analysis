//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Mar 22 15:07:179 2017 by ROOT version 6.09/01
// from TChain ch/individual_run_root_analyzer
//////////////////////////////////////////////////////////

#ifndef individual_run_root_analyzer_h
#define individual_run_root_analyzer_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TObject.h"

class individual_run_root_analyzer {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
 //UVevent         *evt;
   UInt_t          fUniqueID;
   UInt_t          fBits;
   Float_t         WavSampleRate;
   Int_t           WavDimSize;
   Float_t         WavDeltat;
   Int_t           evtno;
   Int_t           fwav_;
   UInt_t          fwav_fUniqueID[17];   //[fwav_]
   UInt_t          fwav_fBits[17];   //[fwav_]
   Int_t           fwav_DoFFT[17];   //[fwav_]
   Int_t           fwav_ReflectionCancelling[17];   //[fwav_]
   Float_t         fwav_Samplerate[17];   //[fwav_]
   Float_t         fwav_Deltat[17];   //[fwav_]
   Int_t           fwav_DimSize[17];   //[fwav_]
   Float_t         fwav_baseline[17];   //[fwav_]
   Float_t         fwav_noiserms[17];   //[fwav_]
   Float_t         fwav_qfast[17];   //[fwav_]
   Float_t         fwav_qtot[17];   //[fwav_]
   Float_t         fwav_peaktime[17];   //[fwav_]
   Float_t         fwav_gain[17];   //[fwav_]
   Float_t         fwav_amp[17];   //[fwav_]
   Double_t        fwav_time[17];   //[fwav_]
   Double_t        fwav_risingtime[17];   //[fwav_]
   Double_t        fwav_fallingtime[17];   //[fwav_]
   Double_t        fwav_TotThreshold[17];   //[fwav_]
   Int_t           fwav_MinimumTot[17];   //[fwav_]
   Int_t           fwav_npeaks[17];   //[fwav_]
   Int_t           fwav_evtno[17];   //[fwav_]
   Int_t           fwav_chno[17];   //[fwav_]
   Int_t           fwav_WavID[17];   //[fwav_]
   Int_t           fwav_TriggerMarker[17];   //[fwav_]
   Double_t        fwav_gmean[17];   //[fwav_]
   Double_t        fwav_gsigma[17];   //[fwav_]
   Double_t        fwav_gpeak[17];   //[fwav_]
   Double_t        fwav_gtime[17];   //[fwav_]
   Double_t        fwav_grisetime[17];   //[fwav_]
   Double_t        fwav_gchi2[17];   //[fwav_]
   Char_t          fwav_WavName[17][100];   //[fwav_]
   Char_t          fwav_SplineName[17][100];   //[fwav_]
   Char_t          fwav_SplineTitle[17][100];   //[fwav_]
   Int_t           fwav_PointsPerSpline[17];   //[fwav_]
   Char_t          fwav_CFDName[17][100];   //[fwav_]
   Char_t          fwav_CFDTitle[17][100];   //[fwav_]
   Char_t          fwav_CFDSplineName[17][100];   //[fwav_]
   Char_t          fwav_CFDSplineTitle[17][100];   //[fwav_]
   vector<float>   fwav_vol[17];   //[fwav_]
   vector<float>   fwav_vol_fft[17];   //[fwav_]
   vector<float>   fwav_vol_cfd[17];   //[fwav_]
   vector<float>   fwav_re_fft[17];   //[fwav_]
   vector<float>   fwav_im_fft[17];   //[fwav_]
   vector<float>   fwav_mag_fft[17];   //[fwav_]
   vector<float>   fwav_mag_processed[17];   //[fwav_]
   Float_t         fwav_TimeStamp[17];
   Int_t           fwav_IfDynamicWindow[17];   //[fwav_]
   Double_t        fwav_FitWindow_min[17];   //[fwav_]
   Double_t        fwav_FitWindow_max[17];   //[fwav_]
   Double_t        fwav_DisWindow_min[17];   //[fwav_]
   Double_t        fwav_DisWindow_max[17];   //[fwav_]
   Float_t         fwav_CutoffFrequency[17];   //[fwav_]
   Float_t         fwav_ReflectionWindow_min[17];   //[fwav_]
   Float_t         fwav_ReflectionWindow_max[17];   //[fwav_]
   Double_t        fwav_AmpThreshold[17];   //[fwav_]
   Double_t        fwav_Fraction_CFD[17];   //[fwav_]
   Int_t           fwav_Delay_CFD[17];   //[fwav_]
   char*           fwav_AnalysisOption[17];   //[fwav_]
   Int_t           fnwav;
   Int_t           ClusterSize;
   Int_t           cutflag;
   vector<float>   t;
   vector<float>   fz;
   Float_t         tabs;
   Float_t         tdiff;
   Float_t         x;
   Float_t         y;

   // List of branches
   TBranch        *b_evt_fUniqueID;   //!
   TBranch        *b_evt_fBits;   //!
   TBranch        *b_evt_WavSampleRate;   //!
   TBranch        *b_evt_WavDimSize;   //!
   TBranch        *b_evt_WavDeltat;   //!
   TBranch        *b_evt_evtno;   //!
   TBranch        *b_evt_fwav_;   //!
   TBranch        *b_fwav_fUniqueID;   //!
   TBranch        *b_fwav_fBits;   //!
   TBranch        *b_fwav_DoFFT;   //!
   TBranch        *b_fwav_ReflectionCancelling;   //!
   TBranch        *b_fwav_Samplerate;   //!
   TBranch        *b_fwav_Deltat;   //!
   TBranch        *b_fwav_DimSize;   //!
   TBranch        *b_fwav_baseline;   //!
   TBranch        *b_fwav_noiserms;   //!
   TBranch        *b_fwav_qfast;   //!
   TBranch        *b_fwav_qtot;   //!
   TBranch        *b_fwav_peaktime;   //!
   TBranch        *b_fwav_gain;   //!
   TBranch        *b_fwav_amp;   //!
   TBranch        *b_fwav_time;   //!
   TBranch        *b_fwav_risingtime;   //!
   TBranch        *b_fwav_fallingtime;   //!
   TBranch        *b_fwav_TotThreshold;   //!
   TBranch        *b_fwav_MinimumTot;   //!
   TBranch        *b_fwav_npeaks;   //!
   TBranch        *b_fwav_evtno;   //!
   TBranch        *b_fwav_chno;   //!
   TBranch        *b_fwav_WavID;   //!
   TBranch        *b_fwav_TriggerMarker;   //!
   TBranch        *b_fwav_gmean;   //!
   TBranch        *b_fwav_gsigma;   //!
   TBranch        *b_fwav_gpeak;   //!
   TBranch        *b_fwav_gtime;   //!
   TBranch        *b_fwav_grisetime;   //!
   TBranch        *b_fwav_gchi2;   //!
   TBranch        *b_fwav_WavName;   //!
   TBranch        *b_fwav_SplineName;   //!
   TBranch        *b_fwav_SplineTitle;   //!
   TBranch        *b_fwav_PointsPerSpline;   //!
   TBranch        *b_fwav_CFDName;   //!
   TBranch        *b_fwav_CFDTitle;   //!
   TBranch        *b_fwav_CFDSplineName;   //!
   TBranch        *b_fwav_CFDSplineTitle;   //!
   TBranch        *b_fwav_TimeStamp;   //!
   TBranch        *b_fwav_vol;   //!
   TBranch        *b_fwav_vol_cfd;   //!
   TBranch        *b_fwav_vol_fft;   //!
   TBranch        *b_fwav_re_fft;   //!
   TBranch        *b_fwav_im_fft;   //!
   TBranch        *b_fwav_mag_fft;   //!
   TBranch        *b_fwav_mag_processed;   //!
   TBranch        *b_fwav_IfDynamicWindow;   //!
   TBranch        *b_fwav_FitWindow_min;   //!
   TBranch        *b_fwav_FitWindow_max;   //!
   TBranch        *b_fwav_DisWindow_min;   //!
   TBranch        *b_fwav_DisWindow_max;   //!
   TBranch        *b_fwav_CutoffFrequency;   //!
   TBranch        *b_fwav_ReflectionWindow_min;   //!
   TBranch        *b_fwav_ReflectionWindow_max;   //!
   TBranch        *b_fwav_AmpThreshold;   //!
   TBranch        *b_fwav_Fraction_CFD;   //!
   TBranch        *b_fwav_Delay_CFD;   //!
   TBranch        *b_fwav_AnalysisOption;   //!
   TBranch        *b_evt_fnwav;   //!
   TBranch        *b_evt_ClusterSize;   //!
   TBranch        *b_evt_cutflag;   //!
   TBranch        *b_evt_t;   //!
   TBranch        *b_evt_fz;   //!
   TBranch        *b_evt_tabs;   //!
   TBranch        *b_evt_tdiff;   //!
   TBranch        *b_evt_x;   //!
   TBranch        *b_evt_y;   //!

   individual_run_root_analyzer(){};
   individual_run_root_analyzer(TTree *tree);
   virtual ~individual_run_root_analyzer();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

