#ifndef gfx_h
#define gfx_h

//______________________________________________________________________________

TGraphErrors* generateGraph( std::vector<Double_t> xx, std::vector<Double_t> yy, std::vector<Double_t> exx, std::vector<Double_t> eyy ){
  //Int_t npoints = xx.size();
  //Double_t* x  = &xx[0];
  //Double_t* ex = &exx[0];
  //Double_t* y  = &yy[0];
  //Double_t* ey = &eyy[0];

  //TGraphErrors* graph = new TGraphErrors(npoints, x, y, ex, ey);
  
  
  
  TGraphErrors* graph = new TGraphErrors(xx.size(), xx.data(), yy.data(), exx.data(), eyy.data());
  graph->SetMarkerStyle(21);

  return graph;
}

//_________________________________________________________________________________________________________

void printPNGandPDF( TCanvas* canvas, string outputName, string outputDir = "", string outputSubDir = ""  ){
  // Print canvas to png to outputDir then prints pdf in outputDir/pdf
  string suffix;
  suffix = ".png";
  canvas->Print( (outputDir + outputSubDir + outputName + suffix).c_str(), suffix.c_str());
  suffix = ".pdf";
  canvas->Print( (outputDir + "pdf/" + outputSubDir + outputName + suffix).c_str(), suffix.c_str());
}

//_________________________________________________________________________________________________________

void drawVLine( Double_t value, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1, Int_t istyle = 8, Float_t pct = 1 ){
  // Draw vertical line on the canvas at a certain x-value, value, from zero to max of that histogram, max

  TLine* line = new TLine( value, min, value, max ); 
  //if( study == "bay_study/" ){
  //  color = kBlue;
  //}
  line->SetLineColorAlpha(color, pct);
  line->SetLineWidth(thickness);
  
  line->SetLineStyle(istyle);
  line->Draw("same");
}

//_________________________________________________________________________________________________________

void drawHLine( Double_t value, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1, Int_t istyle = 8  ){
  // Draw vertical line on the canvas at a certain x-value, value, from zero to max of that histogram, max

  TLine* line = new TLine( min, value, max, value ); 
  //if( study == "bay_study/" ){
  //  color = kBlue;
  //}
  line->SetLineColor(color);
  line->SetLineWidth(thickness);
  
  line->SetLineStyle(istyle);
  line->Draw("same");
}

//_________________________________________________________________________________________________________

void drawVLines( Double_t left, Double_t right, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1, Float_t pct = 0.45){
  // Draw vertical lines to the left and right from zero to max of the histogram

  drawVLine( left , max, min, color, thickness, 2, pct );
  drawVLine( right, max, min, color, thickness, 9, pct );
}

//_________________________________________________________________________________________________________

void drawHLines( Double_t bot, Double_t top, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1){
  // Draw vertical lines to the left and right from zero to max of the histogram

  drawHLine( bot, max, min, color, thickness, 2 );
  drawHLine( top, max, min, color, thickness, 9 );
}

//_________________________________________________________________________________________________________

void drawVLines_2( Double_t left, Double_t right, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1){
  // Draw vertical lines to the left and right from zero to max of the histogram

  drawVLine( left , max, min, color, thickness );
  drawVLine( right, max, min, color, thickness );
}

//_________________________________________________________________________________________________________

void drawHLines_2( Double_t bot, Double_t top, Double_t max, Double_t min = 0, EColor color = kRed, Int_t thickness = 1){
  // Draw vertical lines to the left and right from zero to max of the histogram

  drawHLine( bot, max, min, color, thickness );
  drawHLine( top, max, min, color, thickness );
}

//_________________________________________________________________________________________________________

void stackHistsAndPrint( TH1* hist_pre, TH1* hist, string name, string outputDir = "", string outputSubDir ="", std::vector<Double_t> cuts_x = {}, std::vector<Double_t> cuts_y = {}, std::vector<Double_t> noms = {}){
  // Draw stacks of 1D histograms with the stat box placed nicely
  THStack* stack = new THStack;
  TCanvas * can = new TCanvas;
  TPaveStats* ps = nullptr;
  
  Float_t height = 0.15;
  Float_t y1 = 0.95;
  Float_t y2 = y1-height;
  gStyle->SetOptStat();
  gPad->SetGrid();
  hist_pre->Draw();
  gPad->Update();
  ps = (TPaveStats *) hist_pre->FindObject("stats");
  if( ps != 0 ){
    ps->SetY1NDC(y1);
    ps->SetY2NDC(y2);
  }
  
  
  y1 = y2;
  y2 = y1-height;
  if( hist != 0 ){
    hist->Draw();
    gPad->Update();
    ps = (TPaveStats *) hist->FindObject("stats");
    if( ps != 0 ){
      ps->SetTextColor(kBlue);
      ps->SetY1NDC(y1);
      ps->SetY2NDC(y2);
    }

    hist->SetFillColorAlpha(kBlue, 0.35);

    stack->Add( hist );
  }
  stack->Add( hist_pre );
  stack->SetTitle( hist_pre->GetTitle() );
  stack->Draw("nostack");
  stack->GetXaxis()->SetTitle(hist_pre->GetXaxis()->GetTitle());

  if( cuts_x.size() ){ 
    Double_t x0 = cuts_x[0];
    Double_t x1 = cuts_x[1];
    drawVLines( x0, x1, can->GetUymax(), can->GetUymin(), (EColor) kBlack, 2);
  }
  if( cuts_y.size() ){ 
    Double_t y0 = cuts_y[0];
    Double_t y1 = cuts_y[1];
    drawHLines( y0, y1, can->GetUxmax(), can->GetUxmin(), (EColor) kBlack, 2);
  }
  for( Int_t ii = 0; ii < noms.size(); ii++ ){
    drawVLine( noms[ii], can->GetUymax(), can->GetUymin(), kRed, 2, 1, 0.7);
  }
  
  printPNGandPDF( can, name, outputDir, outputSubDir );
  
}

//_________________________________________________________________________________________________________

void customizeLine( TF1 &f, EColor col = kRed, Int_t istyle = 1, Int_t ithick = 3, Float_t pct = 0.45, Int_t icol = -99){
  f.SetLineColorAlpha( col, pct );
  f.SetLineStyle( istyle );
  f.SetLineWidth( ithick );
  if( icol != -99 ){
    f.SetLineColor( icol );
  }

}

//_________________________________________________________________________________________________________

//void stackHists2AndPrint( TH2* hist_pre, TH2* hist, string name, string outputDir = "", string outputSubDir ="", std::vector<Double_t> cuts_x = {}, std::vector<Double_t> cuts_y = {}, TF1 * f1 = nullptr, TF1 * f2 = nullptr ){
void stackHists2AndPrint( TH2* hist_pre, TH2* hist, string name, string outputDir = "", string outputSubDir ="", std::vector<Double_t> cuts_x = {}, std::vector<Double_t> cuts_y = {}, TF1 * f1 = nullptr, TF1 * f2lo = nullptr, TF1 * f2hi = nullptr ){
  // Draw stacks of 1D histograms with the stat box placed nicely
  TCanvas * can = new TCanvas;
  TPaveStats* ps = nullptr;
  
  gStyle->SetOptStat(0);
  hist_pre->Draw();
  gPad->Update();
  
  Float_t height = 0.20;
  Float_t y1 = 0.95;
  Float_t y2 = y1-height;
  hist_pre->Draw();
  gPad->Update();
  ps = (TPaveStats *) hist_pre->FindObject("stats");
  if( ps != 0 ){
    ps->SetY1NDC(y1);
    ps->SetY2NDC(y2);
  }
  
  //y1 = y2;
  //y2 = y1-height;
  hist->Draw();
  gPad->Update();
  ps = (TPaveStats *) hist->FindObject("stats");
  if( ps != 0 ){
    ps->SetTextColor(kBlue);
    ps->SetY1NDC(y1);
    ps->SetY2NDC(y2);
  }

  TExec *ex1 = new TExec("ex1","gStyle->SetPalette(52);");	// Grey Scale
  TExec *ex2 = new TExec("ex2","gStyle->SetPalette(57);");	// Full Color (kBird)
  
  gStyle->SetPalette(52);
  hist_pre->Draw("col");
  ex2->Draw();
  hist->Draw("colz same");

  if( cuts_x.size() ){ 
    Double_t x0 = cuts_x[0];
    Double_t x1 = cuts_x[1];
    drawVLines( x0, x1, can->GetUymax(), can->GetUymin(), kRed, 3, 0.45);
  }
  if( cuts_y.size() ){ 
    Double_t y0 = cuts_y[0];
    Double_t y1 = cuts_y[1];
    drawHLines( y0, y1, can->GetUxmax(), can->GetUxmin(), (EColor) kBlack, 2);
  }
  if( f1 != 0 && f2lo != 0 && f2hi != 0){
    //Double_t xmin = 0;
    //Double_t xmax = 1;
    //std::vector<Double_t> ps_width =  { 0.0469, 0.6123 };
    //TF1 *f2 = new TF1("f2", getWidth, 0, 6);
    //for( Int_t ii = 0; ii < ps_width.size(); ii++ ){ f2->SetParameter( ii, ps_width[ii] ); }
    ///f1->GetRange(xmin, xmax);
    //TF1* f2lo = new TF1("f2lo", "f1-f2");
    //TF1* f2hi = new TF1("f2hi", "f1+f2");
    customizeLine( *f1 );
    customizeLine( *f2lo, EColor(kRed), 2 );
    //customizeLine( *f2lo );
    customizeLine( *f2hi, EColor(kRed), 8 );
    //customizeLine( *f2hi );

    f1->Draw("same");
    f2lo->Draw("same");
    f2hi->Draw("same");
  }

  hist->Draw("axig same");
  gPad->SetGrid();
  

  printPNGandPDF( can, name, outputDir, outputSubDir );
  
}

//_________________________________________________________________________________________________________

#endif 
