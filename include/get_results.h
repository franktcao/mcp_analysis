#ifndef get_results_h
#define get_results_h

TMultiGraph * buildMGraph( const std::vector<std::vector<double>> &x, const std::vector<std::vector<double>> &y, const TString &mg_name, const TString &mg_title, const std::vector<TString> &g_titles );
void drawMGraph( TMultiGraph & mg, std::vector<double> limits_xlo_yhi = {}, bool setlogY = false );
//void initHists( int run );
void initHists( int run, Run_Settings &settings );
TH1F *h_Gains     ;
TH1F *h_amp       ;
TH1F *h_timing    ;
TH1F *h_pos_res   ;
TH1F *h_rise_time ;
TH1F *h_fall_time ;

TH2I *h2_amp2d ;

void getFitValues( TH1 * h, std::vector<double> &vals );
void getFitValues_2gaus( TH1 * h, std::vector<double> &vals );


#endif
