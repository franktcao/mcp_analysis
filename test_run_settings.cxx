#include "Run_Settings.cxx"
#include "Run_Results.cxx"

void test_run_settings( ){
  
  Run_Settings settings("run_conditions.txt");
  Run_Results results;
  results.SetResults( 1, 2, 3, 4 );
  //cout << results.GetGain() << endl;
  cout << results.GetGain() << endl;
  //settings.LoadInDataTable("run_conditions.txt");
  settings.SetValues( 2008 );
  
  //cout << settings.GetHV() << endl;
  //settings.PrintRow();

  std::vector<int> amp1_runs = settings.GetRunsWithAmplification(1);
  cout <<endl << endl << "1X " << endl;
  settings.PrintRows( amp1_runs );


  cout <<endl << endl << "1X SPE " << endl;
  std::vector<int> amp1_spe_runs = settings.GetRunsWithMode("SPE", amp1_runs);
  settings.PrintRows( amp1_spe_runs );

  //cout << settings.GetRunNumber() << endl;


  return;
}
