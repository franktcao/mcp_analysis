"""
==============================
Create 3D histogram of 2D data
==============================

Demo of a histogram for 2 dimensional data as a bar graph in 3D.
"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
#hist, xedges, yedges = np.histogram2d(x, y, bins=4, range=[[0, 4], [0, 4]])

# 0.03 0.92 0.03        2.27 15.65 3.01         1.18 3.14 0.90
#ENM run   w0ph  w1ph  w2ph  w3ph  w4ph  w5ph  w6ph  w7ph  w8ph HV  BField Angle LL
#     1003   2.73  11.62  5.75  16.04  122.54  28.16  9.43  11.95  12.01 4000 0.13 0 10 
#      1004   2.77  10.17  4.36  14.86  122.96  21.43  8.65  10.40  9.83 4000 1.25 0 10 
#      1005   2.65  9.24  3.89  13.63  111.03  18.60  8.02  9.47  9.00 4000 2.50 0 10 


hist1003 = np.array([[ 2.73, 11.62, 5.75 ], [ 16.04, 122.54, 28.16 ], [ 9.43, 11.95, 12.01  ]] )
hist1004 = np.array([[ 2.77, 10.17, 4.36 ], [ 14.86, 122.96, 21.43 ], [ 8.65, 10.40,  9.83  ]] )
# revised
hist1003 = np.array([[ 3.3,  11.7,  5.8  ], [ 19.5,  124.4,  29.5],   [ 11.1, 12.4,   12.41  ]] )
hist1007 = np.array([[ 3.2,  9.9,   4.2  ], [ 15.8,  112.4,  19.8],   [ 9.4,  9.8,   9.6  ]] )
hist=hist1007
xedges =  np.array([ 0., 1., 2., 3. ] )
#hist1005 = np.array([[ 2.77, 10.17, 4.36 ], [ 14.86, 122.96, 21.43 ], [ 8.65, 10.40,  9.83  ]] )
yedges =  np.array([ 0., 1., 2., 3. ] )

print('hist = ', hist)
print('xedges = ',xedges)
print( 'yedges = ',yedges)
# Construct arrays for the anchor positions of the 9 bars.
# Note: np.meshgrid gives arrays in (ny, nx) so we use 'F' to flatten xpos,
# ypos in column-major order. For numpy >= 1.7, we could instead call meshgrid
# with indexing='ij'.
xpos, ypos = np.meshgrid(xedges[:-1] + 0.25, yedges[:-1] + 0.25)
xpos = xpos.flatten('F')
ypos = ypos.flatten('F')
zpos = np.zeros_like(xpos)

# Construct arrays with the dimensions for the 9 bars.
dx = 0.5 * np.ones_like(zpos)
dy = dx.copy()
dz = hist.flatten()

ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color='b', zsort='average')

plt.show()

