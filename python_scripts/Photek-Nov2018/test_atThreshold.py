def atThres(threshold, aWave ) :
   index=-1
   value=0.0
   for x in aWave :
     index += 1
     if x <= threshold :
        value = aWave[index]
        break
   if index == len(aWave) :
      index = 0
      value = 0.0
   return (index,value)


waves=[]
wave1=[0, 0, -1, -2, -2, -2, -1, 0, 0, 0]
waves.append(wave1)
wave2=[0, 0, -1, -2, -2, -2, -2, -2, -2, -2]
waves.append(wave2)

threshold=-10.
index,value = atThres(threshold,waves[0])
print (waves[0],"index=",index,"thres=",threshold)

threshold=-1.5
index,value = atThres(threshold,waves[0])
print (waves[0],"index=",index,"thres=",threshold)

threshold=-1.5
index,value = atThres(threshold,waves[1])
print (waves[1],"index=",index,"thres=",threshold)

