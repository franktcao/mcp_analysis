import sys

def getSize(fileobject):
    fileobject.seek(0,2) # move the cursor to the end of the file
    size = fileobject.tell()
    return size

if len(sys.argv)==2 : 
	aPath=sys.argv[1]
else	:
	print("Usage: sys.argv[0] pathTo_TR_0_0.dat")
	sys.exit()
#file = open('myfile.bin', 'rb')
filepath=aPath+"/TR_0_0.dat"
file = open(filepath, 'rb')
print("file size = ",getSize(file))
bytes=getSize(file)
numEvs=bytes/4/1024 - 2
print ("Number of events in %s is %d "%(filepath, numEvs))
