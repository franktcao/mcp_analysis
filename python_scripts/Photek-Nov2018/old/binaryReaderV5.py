# -*- coding: utf-8 -*-
# binaryReaderV3.py

from __future__ import print_function

import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import math
from binaryReaderUtilities  import peakdet
from binaryReaderUtilities  import getData, getStats
from binaryReaderUtilities  import rotate, myWavePlot 
from binaryReaderUtilities  import bincenters, generateerrorbars, printTypeOf

from defaultParameters import getDefaultParameters


#================= main code section ======================================
t0=time.time()

#parameters Defaults
HV=3500
Angle=0		#degrees
BField=0.0		#Magnetic field T
LL=1		#light level (npe)
dr0=0; dr1=1000
(HV,BRiels,Angle,LL,dr0,dr1)=getDefaultParameters()
parameterName="BField"

Mode="MPE"
Amp=1

sColumnTitles="#RN   EVs    HV   BField  Amp Mode  meanX meanY ch0 ch1 ch2 ch3 ch4 ch5 ch6 ch7 ch8"
#eg            1006 1000 4000 10.000 100 MPE  10.00 10.00 100.0
sformat=      "%4d  %4d  %4d  %0.3f  %3d %s   %0.2f %0.2f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f \n"
# fx.write("%4d  %4d  %4d  %0.3f  %3d %s   %0.2f %0.2f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f"%(
#           runNumber, numbEvs, HV, BField, Amp, Mode, X, Y, npe0,....)


# (Voltage, BField)
"""start
run2Meta={'Run2001':(2.8,0.0), 'Run2002':(2.8,-1), 'Run2003':(2.8,-1), 

        'Run2004':(3.0,0.25), 'Run2005':(3.0,0.50), 'Run2006':(3.0,0.75),
        'Run2007':(3.0,1.00), 'Run2008':(3.0,1.25), 'Run2009':(3.0,1.45),
        'Run2010':(3.0,0.002),
        'Run2011':(4.0,1.45), 'Run2012':(4.0,1.25), 'Run2013':(4.0,1.00),
        'Run2014':(4.0,0.75), 'Run2015':(4.0,0.50), 'Run2016':(4.0,0.25),
        'Run2017':(4.0,0.002), 'CAEN9999':(2.0, 0.0), 'CAEN1041':(2.0, 0.0),
        'CAEN1042':(2.0, 0.0), 'CAEN1043':(2.0, 0.0), 'CAEN1044':(2.0, 0.0),
        'CAEN1045':(2.0, 0.0), 'CAEN1046':(2.0, 0.0), 'CAEN1047':(2.0, 0.0),
        'CAEN1048':(2.0, 0.0), 'CAEN1049':(2.0, 0.0), 'CAEN1050':(2.0, 0.0), 
        'CAEN1051':(2.0, 0.0), 'CAEN1052':(2.0, 0.0), 'CAEN1053':(2.0, 0.0),
        'CAEN1057':(-6 ,9), 'RUN1001':(-6,9) 
}
"""

#(Run,Voltage,BField,Angle)
run2Meta={'RUN1001':(1001,3300,0.0,0.0)}
# (run,X,Y)
run3Meta={'CAEN1041':(1041,-10,10), 'CAEN1042':(1042,-8,10), 'CAEN1043':(1043,-6,10)
,'CAEN1044':(1044,-4 ,10),  'CAEN1045':(1045,-12,10), 'CAEN1046':(1046,-16,10) 
,'CAEN1047':(1047,-14 ,10), 'CAEN1048':(1048,-16,10), 'CAEN1049':(1049,-16 ,10)   
,'CAEN1050':(1050,-18 ,10), 'CAEN1051':(1051,-20,10), 'CAEN1052':(1052,-22 ,10)  
,'CAEN1053':(1053,-24 ,10), 'CAEN1054':(1054,-4 ,10), 'CAEN1055':(1055,-2 ,10) 
,'CAEN1056':(1056,-0 ,10),                  'CAEN1057':(1057,-6 ,9)
,'RUN1001':(1001,-0,10)}

centers = { 'W0':(-6.0,-6.0), 'W1':(-6.0,9),    'W2':(-6.0,6.0),
            'W3':(0.0,-6.0),  'W4':( 0.0,0.0),  'W5':(0.0,5.0),
            'W6':(6.0, -6.0), 'W7':( 6.0, 0.0), 'W8':(6.0,6.0) }


print ("number of args is %d"%len(sys.argv))
if len(sys.argv) == 7 :
   runNumber = int(sys.argv[1])
   numbEvents = int (sys.argv[2])
   runNumberString = sys.argv[1]
   pathName   = "RUN"+runNumberString
   HV = int(sys.argv[3])
   BField=float(sys.argv[4])
   Amp=int(sys.argv[5])
   Mode=sys.argv[6]
   print ("User asked for %d %d %s "%(numbEvents,runNumber, pathName))
   print ("Using parameters %d %f %d %s "%(HV,BField,Amp,Mode))
else :
   print("Usage: ",sys.argv[0]," runNumber numbEvts HV BField Amp Mode")
   sys.exit()

wavesW0=[]
times=[]
for i in range(1024) : times.append(0.2*i)

if runNumber == 0 :
   f1=open("binaryReader.meanPHs.dat","a")
   f1.write(sColumnTitles+"\n")
   f1.close()
   f2=open("binaryReader.NPEs.dat","a")
   f2.write(sColumnTitles+"\n")
   f2.close()
   print("Place headers on summary files and exit!")
   sys.exit()

wantNevs=numbEvents
print("Read in the binary data files... for %d events."%wantNevs)
# def getData( filename, Nev, option=0)
print("Reading wave_0")
wavesW0=getData(pathName+"/wave_0.dat", wantNevs,option=1)
print("Reading wave_1")
wavesW1=getData(pathName+"/wave_1.dat", wantNevs,option=1)
print("Reading wave_2")
wavesW2=getData(pathName+"/wave_2.dat", wantNevs,option=1)
print("Reading wave_3")
wavesW3=getData(pathName+"/wave_3.dat", wantNevs,option=1)
print("Reading wave_4")
wavesW4=getData(pathName+"/wave_4.dat", wantNevs,option=1)
print("Reading wave_5")
wavesW5=getData(pathName+"/wave_5.dat", wantNevs,option=1)
print("Reading wave_6")
wavesW6=getData(pathName+"/wave_6.dat", wantNevs,option=1)
print("Reading wave_7")
wavesW7=getData(pathName+"/wave_7.dat", wantNevs,option=1)
print("Reading wave_8")
wavesW8=getData(pathName+"/wave_8.dat", wantNevs,option=1)

print("Reading wave_Tr0")
wavesTr0=getData(pathName+"/TR_0_0.dat", wantNevs,option=1)
print("Reading wave_Tr1")
wavesTr1=getData(pathName+"/TR_0_1.dat", wantNevs,option=1)      # 
print("Done Reading wave data")

wfms = [wavesW0, wavesW1, wavesW2,  wavesW3,  wavesW4,  wavesW5,  wavesW6, wavesW7, wavesW8, wavesTr0, wavesTr1]
print("length of wavesW0 is %d"%len(wavesW0))
print("length of 1st waveform in wavesW0 is %d"%len(wavesW0[0]))
indxTr0=9
indxTr1=10
ir0=420; ir1=620	# region of signal events
ir0=0; ir1=1000		# region of signal events
ir0=dr0; ir1=dr1	# region of signal events
print('iev tdx wavesTr0[iev][tdx]')
"""start
# debugging
for iev in range(3) :
        for tdx in range(ir0,ir1) :
                print('%d %d     %0.2f'%( iev, tdx,  wavesTr0[iev][tdx]))
"""

iev=0
""" start
print 'raw for waves4'
print 'iev itdx wavesW4'
for itdx in range(520,550):
        print iev,itdx,wavesW4[iev][itdx]
print
iev=1
print 'raw for waves4'
print 'iev itdx wavesW4'
for itdx in range(520,550):
        print iev,itdx,wavesW4[iev][itdx]
print
"""

waveNumb = 0
nev = 10
voffset = 20
voffset = 10
wantRawWfmPlots=True
wantRawWfmPlots=False
if wantRawWfmPlots :
    print("Plot raw over ir0,ir1 = ",ir0,ir1)
    # def myWavePlot( id, aTitle, times, waves, waveNumb, nev, voffset, pathname=".", is0=0, is1=1000 )
    myWavePlot( 0, "raw W0", times, wavesW0, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 1, "raw W1", times, wavesW1, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 2, "raw W2", times, wavesW2, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 3, "raw W3", times, wavesW3, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 4, "raw W4", times, wavesW4, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 5, "raw W5", times, wavesW5, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 6, "raw W6", times, wavesW6, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 7, "raw W7", times, wavesW7, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 8, "raw W8", times, wavesW8, waveNumb, nev, voffset,pathname=".",is0=ir0,is1=ir1 )
    myWavePlot( 9, "raw TR0", times, wavesTr0, waveNumb, nev, 100.0,pathname=".",is0=ir0,is1=ir1 )


wavesTr0rot=[]
wavesTr1rot=[]
wavesW0rot=[]
wavesW1rot=[]
wavesW2rot=[]
wavesW3rot=[]
wavesW4rot=[]
wavesW5rot=[]
wavesW6rot=[]
wavesW7rot=[]
wavesW8rot=[]

thresholdTr=-100.0      # mV
thresholdWs=-4.0        # mV
peakBin=615             # time bin to align to
peakBin=580             # time bin to align to
print("Using thresholdTr,thresholdWs,peakBin = ", thresholdTr,thresholdWs,peakBin)
WTr0timebins=[]; WTr0phs=[]
W0timebins=[]; W0phs=[]; W0Aphs=[]
W1timebins=[]; W1phs=[]; W1Aphs=[]
W2timebins=[]; W2phs=[]; W2Aphs=[]
W3timebins=[]; W3phs=[]; W3Aphs=[]
W4timebins=[]; W4phs=[]; W4Aphs=[]
W5timebins=[]; W5phs=[]; W5Aphs=[]
W6timebins=[]; W6phs=[]; W6Aphs=[]
W7timebins=[]; W7phs=[]; W7Aphs=[]
W8timebins=[]; W8phs=[]; W8Aphs=[]

WTr0timebins=[]; WTr0phs=[]
WTr1timebins=[]; WTr1phs=[]
W0peakCnts=[]; W0peakCharges=[]
W1peakCnts=[]; W1peakCharges=[]
W2peakCnts=[]; W2peakCharges=[]
W3peakCnts=[]; W3peakCharges=[]
W4peakCnts=[]; W4peakCharges=[]
W5peakCnts=[]; W5peakCharges=[]
W6peakCnts=[]; W6peakCharges=[]
W7peakCnts=[]; W7peakCharges=[]
W8peakCnts=[]; W8peakCharges=[]


lowBin = 15; hghBin= 26
lowBin = 50; hghBin= 50
peakLocs=[]
print('thresholdTr = ',thresholdTr)
for iev in range(len(wavesTr0)) :
        if iev%100 == 0 : print("Working on event %d" % iev)
        # get information for trigger waveform
        awaveTr0 = wavesTr0[iev]
        maxtabTr0, mintabTr0 = peakdet(awaveTr0,-thresholdTr)
        #print("iev=%d"%iev)
        #print("length of maxtabTr0 is ",len(maxtabTr0))
        #print("length of mintabTro is ",len(mintabTr0))
        #print("maxtabTr0=", maxtabTr0)
        #print( "mintabTr0=",mintabTr0)
        if len(mintabTr0) != 0 :
                Tr0peakTimeEst = 0.200*mintabTr0[0][0]
                Tr0peakSizeEst = -mintabTr0[0][1]
                peakLoc = int(mintabTr0[0][0])
                WTr0timebins.append(Tr0peakTimeEst)
                WTr0phs.append(Tr0peakSizeEst)
        else :
                Tr0peakTimeEst = 0
                Tr0peakSizeEst = 0
                peakLoc = 0
                WTr0timebins.append(0)
                WTr0phs.append(0)
        peakLocs.append(peakLoc)
        binOffset = peakLoc - peakBin
        if iev<2 and len(mintabTr0) > 0  : 
                print("Tr0 peakLoc,phs,binOffset = ",peakLoc,-mintabTr0[0][1],binOffset)
                print("Tr0 Tr0peakTimeEst, Tr0peakSizeEst  = ",Tr0peakTimeEst, Tr0peakSizeEst)
        awaveTr0rot = rotate( awaveTr0, -binOffset )
        wavesTr0rot.append( awaveTr0rot )

        awaveW0 = wavesW0[iev]
        awaveW0rot = rotate( awaveW0, -binOffset )
        wavesW0rot.append(awaveW0rot)
        maxtabW0, mintabW0 = peakdet(awaveW0rot,-thresholdWs)
        W0peakCnts.append(len(mintabW0))
        if len(mintabW0)>0 :
                W0timebins.append(0.200*mintabW0[0][0])
                W0phs.append(-mintabW0[0][1])
        else :
                W0timebins.append(0)
                W0phs.append(0.0)

        awaveW1 = wavesW1[iev]
        awaveW1rot = rotate( awaveW1, -binOffset )
        wavesW1rot.append(awaveW1rot)
        maxtabW1, mintabW1 = peakdet(awaveW1rot,-thresholdWs)
        W1peakCnts.append(len(mintabW1))
        if len(mintabW1)>0 :
                W1timebins.append(0.200*mintabW1[0][0])
                W1phs.append(-mintabW1[0][1])
        else :
                W1timebins.append(0)
                W1phs.append(0.0)

        awaveW2 = wavesW2[iev]
        awaveW2rot = rotate( awaveW2, -binOffset )
        wavesW2rot.append(awaveW2rot)
        maxtabW2, mintabW2 = peakdet(awaveW2rot,-thresholdWs)
        W1peakCnts.append(len(mintabW2))
        if len(mintabW2)>0 :
                W2timebins.append(0.200*mintabW2[0][0])
                W2phs.append(-mintabW2[0][1])
        else :
                W2timebins.append(0)
                W2phs.append(0.0)
        #if len(mintabW0)>0 and len(mintabW2)>0 :
        #       W0mW2timebins.append(0.200*(mintabW0[0][0]-mintabW2[0][0]))
        #else :
        #       W0mW2timebins.append(-10)

        awaveW3 = wavesW3[iev]
        awaveW3rot = rotate( awaveW3, -binOffset )
        wavesW3rot.append(awaveW3rot)
        maxtabW3, mintabW3 = peakdet(awaveW3rot,-thresholdWs)
        W3peakCnts.append(len(mintabW3))
        if len(mintabW3)>0 :
                W3timebins.append(0.200*mintabW3[0][0])
                W3phs.append(-mintabW3[0][1])
        else :
                W3timebins.append(0)
                W3phs.append(0.0)

        awaveW4 = wavesW4[iev]
        awaveW4rot = rotate( awaveW4, -binOffset )
        wavesW4rot.append(awaveW4rot)
        maxtabW4, mintabW4 = peakdet(awaveW4rot,-thresholdWs)
        W4peakCnts.append(len(mintabW4))
        if len(mintabW4)>0 :
                W4peakTimeEst = 0.200*mintabW4[0][0]
                W4peakSizeEst = -mintabW4[0][1]
                W4timebins.append(0.200*mintabW4[0][0])
                W4phs.append(-mintabW4[0][1])
        else :
                W4peakTimeEst = 0
                W4peakSizeEst = 0
                W4timebins.append(0)
                W4phs.append(0.0)
        W4min=np.min(awaveW4rot)
        W4Aphs.append(W4min)
        if iev<2 : 
                print("W4 W4peakTimeEst, W4peakSizeEst  = ",W4peakTimeEst, W4peakSizeEst)

        awaveW5 = wavesW5[iev]
        awaveW5rot = rotate( awaveW5, -binOffset )
        wavesW5rot.append(awaveW5rot)
        maxtabW5, mintabW5 = peakdet(awaveW5rot,-thresholdWs)
        W5peakCnts.append(len(mintabW5))
        if len(mintabW5)>0 :
                W5timebins.append(0.200*mintabW5[0][0])
                W5phs.append(-mintabW5[0][1])
        else :
                W5timebins.append(0)
                W5phs.append(0.0)

        awaveW6 = wavesW6[iev]
        awaveW6rot = rotate( awaveW6, -binOffset )
        wavesW6rot.append(awaveW6rot)
        maxtabW6, mintabW6 = peakdet(awaveW6rot,-thresholdWs)
        if len(mintabW6)>0 :
           W6timebins.append(0.200*mintabW6[0][0]);W6phs.append(-mintabW6[0][1])
        else :
           W6timebins.append(0); W6phs.append(0.0)

        awaveW7 = wavesW7[iev]
        awaveW7rot = rotate( awaveW7, -binOffset )
        wavesW7rot.append(awaveW7rot)
        maxtabW7, mintabW7 = peakdet(awaveW7rot,-thresholdWs)
        W7peakCnts.append(len(mintabW7))
        if len(mintabW7)>0 :
                W7timebins.append(0.200*mintabW7[0][0])
                W7phs.append(-mintabW7[0][1])
        else :
                W7timebins.append(0)
                W7phs.append(0.0)

        awaveW8 = wavesW8[iev]
        awaveW8rot = rotate( awaveW8, -binOffset )
        wavesW8rot.append(awaveW8rot)
        maxtabW8, mintabW8 = peakdet(awaveW8rot,-thresholdWs)
        W8peakCnts.append(len(mintabW8))
        if len(mintabW8)>0 :
                W8timebins.append(0.200*mintabW8[0][0])
                W8phs.append(-mintabW8[0][1])
        else :
                W8timebins.append(0)
                W8phs.append(0.0)

        if len( mintabW4 ) > 0 :
           W4peakCharge=0
           ipeak=0
           ipeakG80 =  0
           while ipeak < len(mintabW4) :
                if 0.2*mintabW4[ipeak][0] > 80.0 : 
                   ipeakG80 += 1 
                   break
                ipeak += 1
           if ipeakG80 > 0 :
              #print "iplow, iphgh = ",int(mintabW0[ipeak][0])-15, 
              #int(mintabW0[ipeak][0])+26
              iplow = max(0, int(mintabW4[ipeak][0])-lowBin)
              iphgh = min(int(mintabW4[ipeak][0])+hghBin, 1023)
              for ip in range(iplow, iphgh ) :
                 #W4peakCharge += -0.2 * awaveW4rot[ip]         #nsec*mV
                 W4peakCharge += -0.2 * awaveW4rot[ip] / 5      # pCoul
              W4peakCharges.append(W4peakCharge)
           else :
              W4peakCharges.append(0)
        else :
           W4peakCharges.append(0)

# end of event loop

wX  = np.mean(W0phs) * centers['W0'][0] + np.mean(W1phs) * centers['W1'][0] + np.mean(W2phs) * centers['W2'][0]
wX += np.mean(W3phs) * centers['W3'][0] + np.mean(W4phs) * centers['W4'][0] + np.mean(W5phs) * centers['W5'][0]
wX += np.mean(W6phs) * centers['W6'][0] + np.mean(W7phs) * centers['W7'][0] + np.mean(W8phs) * centers['W8'][0]
wX = wX/( np.mean(W0phs) + np.mean(W1phs) + np.mean(W2phs) + np.mean(W3phs) + np.mean(W4phs) + np.mean(W5phs) + np.mean(W6phs) + np.mean(W7phs) + np.mean(W8phs) )
wY  = np.mean(W0phs) * centers['W0'][1] + np.mean(W1phs) * centers['W1'][1] + np.mean(W2phs) * centers['W2'][1]
wY += np.mean(W3phs) * centers['W3'][1] + np.mean(W4phs) * centers['W4'][1] + np.mean(W5phs) * centers['W5'][1]
wY += np.mean(W6phs) * centers['W6'][1] + np.mean(W7phs) * centers['W7'][1] + np.mean(W8phs) * centers['W8'][1]
wY = wY/( np.mean(W0phs) + np.mean(W1phs) + np.mean(W2phs) + np.mean(W3phs) + np.mean(W4phs) + np.mean(W5phs) + np.mean(W6phs) + np.mean(W7phs) + np.mean(W8phs) )
print('ENM X = ',wX, 'Y = ', wY)

f1=open("binaryReader.meanPHs.dat","a")

f1.write(sformat%(
runNumber, numbEvents, HV, BField, Amp, Mode, wX, wY, np.mean(W0phs), np.mean(W1phs), np.mean(W2phs), 
np.mean(W3phs),np.mean(W4phs),np.mean(W5phs),np.mean(W6phs),np.mean(W7phs),np.mean(W8phs)
))
f1.close()

"""start
for iev in range(len(WTr0timebins)) :
        print("%d %0.2f %0.2f %0.2f %0.2f %0.2f %0.2f"%(iev,WTr0timebins[iev],
        WTr0phs[iev],W4timebins[iev],W4phs[iev],W7timebins[iev],W7phs[iev] ))

        if iev < 100 :
           print 'ev ipeak mintabW0[ipeak][0], mintabW0[ipeak][0], W0peakCharge'
           print iev,ipeak,mintabW0[ipeak][0], mintabW0[ipeak][0], W0peakCharge
"""

"""start
testCnt = 0
for i in range(len(W0phs)) :
    testThreshold=1.0
    if W0phs[i] > testThreshold :
        testCnt += 1
        aTitle= pathName+' waves W0 rotated at threshold > %f ' % testThreshold
        myWavePlot( 10,aTitle,times,wavesW0rot, i, 1, 0.0, pathName)
        print 'evnt, w0 ph =', i , W0phs[i]
    if testCnt > 3 : break
    plt.show()
"""
 

wantrotWaveformPlots=False
wantrotWaveformPlots=False
if wantrotWaveformPlots :
   print("Saving rotated plots with waveNumb, nev, voffset = ",waveNumb, nev, voffset)
   print('Using pathName =',pathName)
   myWavePlot(  10, pathName+' waves W0 rotated', times, wavesW0rot, waveNumb, nev, voffset, pathName)
   myWavePlot(  11, pathName+' waves W1 rotated', times, wavesW1rot, waveNumb, nev, voffset, pathName)
   myWavePlot(  12, pathName+' waves W2 rotated', times, wavesW2rot, waveNumb, nev, voffset, pathName)
   myWavePlot(  13, pathName+' waves W3 rotated', times, wavesW3rot, waveNumb, nev, voffset, pathName)
   myWavePlot(  14, pathName+' waves W4 rotated', times, wavesW4rot, waveNumb, nev, voffset, pathName)
   myWavePlot(  15, pathName+' waves W5 rotated', times, wavesW5rot, waveNumb, nev, voffset, pathName)
   myWavePlot(  16, pathName+' waves W6 rotated', times, wavesW6rot, waveNumb, nev, voffset,   pathName)
   myWavePlot(  17, pathName+' waves W7 rotated', times, wavesW7rot, waveNumb, nev, voffset,   pathName)
   myWavePlot(  18, pathName+' waves W8 rotated', times, wavesW8rot, waveNumb, nev, voffset,   pathName)
   myWavePlot(  19, pathName+' waves Tr0 rotated', times, wavesTr0rot, waveNumb, nev, 100.0,   pathName)



useLogscale=False
useLogscale=True
#aInd = np.argmin(wave)
#print 'minimum of wave is ', np.min(wave), ' at index = ', aInd, ' time = ', 0.2*aInd
#minTimes.append( 0.2*aInd )
#waves.append( wave )

print("101 Historgram number of negative peaks found by peakdet()")
plt.figure(101)
h1=np.histogram(W4peakCnts, bins=10)
values = h1[0]
centers=bincenters(h1[1])
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
plt.title(pathName+" W4 number of peaks"  )
plt.xlabel('number of peaks')
meanNumPksW4=np.mean(W4peakCnts)
plt.savefig(pathName+"/101-W4-histo-numberOfNegPeaks.png")

print("\n\n102 Histogram W4 time from peakdet")
plt.figure(102)
#numberOfBins = 200
#h2=np.histogram(W4timebins, bins=numberOfBins)
numberOfBins = 50
bins=np.linspace(180,190,numberOfBins)
h2=np.histogram(W4timebins, bins)
values = h2[0]
centers=bincenters(h2[1])
#print("bin width = ", (centers[numberOfBins-1] - centers[0])/numberOfBins)
delta = (centers[1]-centers[0])
print('delta time bin (ns)is ', delta)
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
(W4lenght, W4min, W4max, W4mean, W4std, methodUsed)=getStats( 'Time W4', W4timebins, 
0.0, 200.0 )
plt.title(pathName+" W4 time mean=%0.3f  est rms=%0.3f (ns)"%(W4mean,W4std))
plt.xlabel('Time (ns)')
peakThres = 3.0
maxtabW4time, mintabW4time = peakdet(values,peakThres)
numbPosPeaks = len(maxtabW4time) 
#for i in range(numbPosPeaks) :
#        print(i,maxtabW4time[i][0], maxtabW4time[i][0]*delta, "(ns)",maxtabW4time[i][1])
if len(maxtabW4time) != 0 :
        peakLoc = maxtabW4time[0][0]
        peakHeight = int(maxtabW4time[0][1])
else :
        peakLoc = 0
        peakHeight = 0
#print("W4 time: numbPosPeaks, peakLoc, peakHeight = ",numbPosPeaks, peakLoc, peakHeight)
getStats( 'Time W4', W4timebins, 50.0, 200.0 )
getStats( 'Time W4', W4timebins, 182.0, 186.0 )
plt.savefig(pathName+"/102-histo-W4-time-from-peakdet.png")

""" start
plt.figure(103)
bins=np.linspace(-15,+15,150)
h3=np.histogram(W0mW2timebins, bins)
values = h3[0]
centers=bincenters(h3[1])
print 'delta time bin (ns)is ', (centers[1]-centers[0])*0.200
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
(W0mW2lenght, W0mW2min, W0mW2max, W0mW2mean, W0mW2std,methodUsed)=getStats(
 'Time W0-W2', W0mW2timebins, 50.0, 150.0 )
plt.title(pathName+" W0-W2 time  est rms=%0.3f (ns)"%W0std)
plt.xlabel('Time (ns)')
plt.savefig(pathName+"/103-histo-W0-W2-time-deference-from-peakdet.png")
"""
getStats( 'Time W6', W6timebins, 50.0, 150.0 )


print("\n\nHistogram W4 pulse heights")
plt.figure(104)
h4=np.histogram(W4phs, bins=100)
values = h4[0]
centers=bincenters(h4[1])
print('delta ph bin is ', centers[1]-centers[0])
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
getStats( 'ph W4', W4phs, 0.0, 200.0 )
meanPhW4 = np.mean(W4phs)
npeW4 = ( np.mean(W4phs)/(np.std(W4phs)+0.000001) )**2
plt.title(pathName+" W4 pulse height est npe=%0.3f"%npeW4)
plt.xlabel('pulse height (mV)')
plt.savefig(pathName+"/104-histo-W4-pulse-height.png")
(W4No0pHlength,W4No0pHmin,W4No0pHmax,W4No0phmean,W4No0phstd,methodUsed)=getStats( 
'W4 No0 Ph', W4phs, 0.1, 200.0, 1, 1 )
print("W4ph (no 0s) count mean, std, method  = ", W4No0pHlength,W4No0phmean, W4No0phstd, methodUsed)



getStats( 'ph W0', W0phs, 0.0, 200.0 )
npeW0 = ( np.mean(W0phs)/(np.std(W0phs)+0.000001) )**2
getStats( 'ph W1', W1phs, 0.0, 200.0 )
npeW1 = ( np.mean(W1phs)/(np.std(W1phs)+0.000001) )**2
getStats( 'ph W2', W2phs, 0.0, 200.0 )
npeW2 = ( np.mean(W2phs)/(np.std(W2phs)+0.000001) )**2
getStats( 'ph W3', W3phs, 0.0, 200.0 )
npeW3 = ( np.mean(W3phs)/(np.std(W3phs)+0.000001) )**2
getStats( 'ph W4', W4phs, 0.0, 200.0 )
npeW4 = ( np.mean(W4phs)/(np.std(W4phs)+0.000001) )**2
getStats( 'ph W5', W5phs, 0.0, 200.0 )
npeW5 = ( np.mean(W5phs)/(np.std(W5phs)+0.000001) )**2
getStats( 'ph W6', W6phs, 0.0, 200.0 )
npeW6 = ( np.mean(W6phs)/(np.std(W6phs)+0.000001) )**2
getStats( 'ph W7', W7phs, 0.0, 200.0 )
npeW7 = ( np.mean(W7phs)/(np.std(W7phs)+0.000001) )**2
getStats( 'ph W8', W8phs, 0.0, 200.0 )
npeW8 = ( np.mean(W8phs)/(np.std(W8phs)+0.000001) )**2

print("ENM  npeW0 npeW1 npeW2 npeW3 npeW4 npeW5 npeW6 npeW7 npeW8")
print("ENM  %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f"%(
 npeW0, npeW1, npeW2, npeW3, npeW4, npeW5, npeW6,npeW7, npeW8))

f2=open("binaryReader.NPEs.dat","a")
f2.write(sformat%( runNumber,numbEvents, HV, BField, Amp, Mode, wX, wY, 
npeW0, npeW1, npeW2, npeW3, npeW4, npeW5, npeW6,npeW7, npeW8 
)) 
f2.close()

plt.figure(105)
h5=np.histogram(W4peakCharges, bins=50)
values = h5[0]
centers=bincenters(h5[1])
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
plt.title(pathName+" W4 peak total Charge "  )
plt.xlabel('Charge (pCoul)')
meanChargeW4=np.mean(W4peakCharges)
stdChargeW4=np.std(W4peakCharges)+0.0000001	#enm force non-zero
npeChargeW4= (meanChargeW4/stdChargeW4)**2
plt.title(pathName+" W4 peak total meanCharge = %f  npe = %f " % (meanChargeW4,npeChargeW4  ))
plt.savefig(pathName+"/105-histo-W4-peak-total-charge.png")
print("meanChargeW4, npeChargeW4 = ",meanChargeW4, npeChargeW4)


print('pathName = ', pathName)
print('run2Meta = ', run2Meta)



"""start
f1.write("# Run nevts Volt BField mNumPksW0 mPhW0 npeW0 tW0mean tW0std mCharge npeCharge W0No0pHlength,W0No0phmean, W0No0phstd\n")
f1.write("%s %d %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %d %0.3f %0.3f\n" % ( 
                                        pathName, wantNevs, 
                                        run2Meta[pathName][0],run2Meta[pathName][1], 
                                        meanNumPksW0, meanPhW0, 
                                        npeW0, W0mean, W0std, 
                                        meanChargeW0, npeChargeW0,
                                        W0No0pHlength,W0No0phmean, W0No0phstd  ))
"""
#print 'Exit early due problems with metadata!'

print("Stopping early"); exit()

#======================================================================================

print("# Run nevts Volt BField mNumPksW0 mPhW0 npeW0 tW0mean tW0std mCharge npeCharge W0No0pHlength,W0No0phmean, W0No0phstd\n")
print(("%s %d %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %d %0.3f %0.3f \n" % ( 
                                        pathName, wantNevs, 
                                        run2Meta[pathName][0],run2Meta[pathName][1], 
                                        meanNumPksW0, meanPhW0, 
                                        npeW0, W0mean, W0std, 
                                        meanChargeW0, npeChargeW0,
                                        W0No0pHlength,W0No0phmean, W0No0phstd  )))
#plt.show()

t1=time.time()
print("Job time %0.2f (sec)"%(t1-t0))
sys.exit()
        
print('linecount = ',  linecount)


wavesW0rot=[]
wavesTrrot=[]
for i in range(len(waves)) :
        awaveW0 = waves[i]
        offset = TrBinOffsets[i]
        awaveW0rot = rotate( awaveW0, -offset )
        wavesW0rot.append( awaveW0rot )
        awaveTr = wavesTr[i]
        awaveTrrot = rotate( awaveTr, -offset )
        wavesTrrot.append( awaveTrrot )

from operator import add
#map(add, list1, list2)

#sumW0 = [0]*1024
#print 'length of sumW0 = ', len(sumW0)
#for i in range(len(wavesW0rot)) :
#       sumW0 = map(add, sumW0, wavesW0rot[i] )

# collect data into 3 groups of 30 triggers
sumW0s = []
aveW0s = []
for grp in range(0,3) :
   sumW0 = [0]*1024
   for i in range( 30*grp, 30*(grp+1) ) :
        sumW0 = list(map(add, sumW0, wavesW0rot[i] ))
   sumW0s.append(sumW0)
   aveW0 = newList = [x / 30 for x in sumW0]
   aveW0s.append(aveW0)



waveNumb = 25
voffset = 10.0 # mV
voffset = 100.0 # mV
nev = 25
print("in main: length of times is ", len(times)," length of waves is ", len(waves))
voffset = 10.0 # mV
#voffset = 50.0 # mV
#myWavePlot( 100, times, waves, waveNumb, nev, voffset )
myWavePlot( 100, times, wavesW0rot, waveNumb, nev, voffset )

voffset = 100.0 # mV
#myWavePlot( 101, times, wavesTr, waveNumb, nev, voffset )
myWavePlot( 101, times, wavesTrrot, waveNumb, nev, voffset )

voffset=10.
#voffset=100.
myWavePlot( 102, times, sumW0s , 0, 3, 30*voffset  )
myWavePlot( 103, times, aveW0s , 0, 3, voffset  )

plt.show()

sys.exit()

text_file = open("binaryReader.dat", "r")
#lines = text_file.readlines()
lines = text_file.read().split(' ')
print(lines[0])
print('length of lines is ',len(lines))
print('length of line is ',len(lines[0]))
print('lines[-1] = ', lines[-1], "\n = lines[-1]")
wave=[]
waves=[]
for i in range(len(lines)) :
        wave=[]
        aLine=lines[i]
        print('length of aLine = ',len(aLine))
        for j in range(len(aLine)) :
                print(j, aLine[j])
                wave.append( float(aLine[j] ) )
        waves.append(wave)
print(wave)
print('length of wave is ', len(wave))
print('length of waves is ', len(waves))
text_file.close()

sys.exit()

with open('special.dat') as f:
    for line in f:
        #inner_list = [elt.strip() for elt in line.split(' ')]
        # in alternative, if you need to use the file content as numbers
        inner_list = [int(elt.strip()) for elt in line.split(' ')]
        list_of_lists.append(inner_list)
print('length of list_of_lists is ', len(list_of_lists ))

