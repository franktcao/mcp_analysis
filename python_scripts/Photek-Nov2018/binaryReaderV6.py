# -*- coding: utf-8 -*-
# binaryReaderV6.py

from __future__ import print_function

import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import math
from binaryReaderUtilities  import peakdet
from binaryReaderUtilities  import getData, getStats
from binaryReaderUtilities  import rotate, myWavePlot 
from binaryReaderUtilities  import bincenters, generateerrorbars, printTypeOf

from defaultParameters import getDefaultParameters

def atThres(threshold, aWave ) :
   index=-1
   value=0.
   for x in aWave :
     index += 1
     if x <= threshold :
        value = aWave[index]
        break
   if index == len(aWave) :
      index = 0
      value = 0.0
   return (index,value)



#================= main code section ======================================
print("================================Begin New Run=============================")
t0=time.time()

#parameters Defaults
HV=3500
Angle=0       #degrees
BField=0.0    #Magnetic field T
LL=1          #light level (npe)
dr0=0; dr1=1000
#(HV,BRiels,Angle,LL,dr0,dr1)=getDefaultParameters()
parameterName="BField"

Mode="MPE"
Amp=1

sColumnTitles="#RN  EVs  HV   BField Amp Mode meanX meanY   ch0   ch1   ch2   ch3   ch4   ch5   ch6   ch7   ch8 ch4MeanCharge"
#eg            1006 1000 4000 10.000 100 MPE  10.00 10.00 100.0
sformat=      "%4d  %4d  %4d  %0.3f  %3d %s   %0.2f %0.2f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %f \n"
# fx.write("%4d  %4d  %4d  %0.3f  %3d %s   %0.2f %0.2f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f %0.1f"%(
#           runNumber, numbEvs, HV, BField, Amp, Mode, X, Y, npe0,....)
#print('{0:3d} {1:7.2f} '.format(myInts[i], myFloats[i]))
s2format='{0:4d} {1:4d} {2:4d} {3:5.2f} {4:3d} {5:s} {6:6.2f} {7:6.2f} {8:5.1f} {9:5.1f} {10:5.1f} {11:5.1f} {12:5.1f} {13:5.1f} {14:5.1f} {15:5.1f} {16:5.1f} {17:5.1f}'

# units are mm
centers = { 'W0':(-6.0,-6.0), 'W1':(-6.0,9),    'W2':(-6.0,6.0),
            'W3':(0.0,-6.0),  'W4':( 0.0,0.0),  'W5':(0.0,5.0),
            'W6':(6.0, -6.0), 'W7':( 6.0, 0.0), 'W8':(6.0,6.0) }

centers = { 'W0':(-6.0, -6.0), 'W1':(-6.0, 0.0), 'W2':(-6.0, 6.0),
            'W3':( 0.0, -6.0), 'W4':( 0.0, 0.0), 'W5':( 0.0, 6.0),
            'W6':( 6.0, -6.0), 'W7':( 6.0, 0.0), 'W8':( 6.0, 6.0) }

# get the command line arguements
print ("number of args is %d"%len(sys.argv))
if len(sys.argv) == 7 :
   runNumber = int(sys.argv[1])
   numbEvents = int (sys.argv[2])
   runNumberString = sys.argv[1]
   pathName   = "RUN"+runNumberString
   HV = int(sys.argv[3])
   BField=float(sys.argv[4])
   Amp=int(sys.argv[5])
   Mode=sys.argv[6]
   print ("User asked for %d %d %s "%(numbEvents,runNumber, pathName))
   print ("Using parameters %d %f %d %s "%(HV,BField,Amp,Mode))
else :
   print("Usage: ",sys.argv[0]," runNumber numbEvts HV BField Amp Mode")
   sys.exit()

sys.stderr.write("Processing %s\n"%pathName)

# setup list of times in nsec
times=[]
for i in range(1024) : times.append(0.2*i)

if runNumber == 0 :
   f1=open("binaryReader.meanPHs.dat","a")
   f1.write(sColumnTitles+"\n")
   f1.close()
   f2=open("binaryReader.NPEs.dat","a")
   f2.write(sColumnTitles+"\n")
   f2.close()
   print("Place headers on summary files and exit!")
   sys.exit()

wantNevs=numbEvents
print("Read in the binary data files... for %d events."%wantNevs)
# def getData( filename, Nev, option=0)
print("Reading wave_0")
wavesW0=getData(pathName+"/wave_0.dat", wantNevs,option=1)
print("Reading wave_1")
wavesW1=getData(pathName+"/wave_1.dat", wantNevs,option=1)
print("Reading wave_2")
wavesW2=getData(pathName+"/wave_2.dat", wantNevs,option=1)
print("Reading wave_3")
wavesW3=getData(pathName+"/wave_3.dat", wantNevs,option=1)
print("Reading wave_4")
wavesW4=getData(pathName+"/wave_4.dat", wantNevs,option=1)
print("Reading wave_5")
wavesW5=getData(pathName+"/wave_5.dat", wantNevs,option=1)
print("Reading wave_6")
wavesW6=getData(pathName+"/wave_6.dat", wantNevs,option=1)
print("Reading wave_7")
wavesW7=getData(pathName+"/wave_7.dat", wantNevs,option=1)
print("Reading wave_8")
wavesW8=getData(pathName+"/wave_8.dat", wantNevs,option=1)

print("Reading wave_Tr0")
wavesTr0=getData(pathName+"/TR_0_0.dat", wantNevs,option=1)
print("Reading wave_Tr1")
wavesTr1=getData(pathName+"/TR_0_1.dat", wantNevs,option=1)      # 
print("Done Reading wave data")

noiseRMSs=[]
noiseW4rms=[]
for iWindex in range(0,10) :
        aW4rms = np.std(wavesW4[iWindex][0:128])
        noiseW4rms.append(aW4rms)
meanW4rms = np.mean(noiseW4rms)
print("noiseW4rms = ",noiseW4rms,"mean = ",meanW4rms)
f0=open('noise.dat','a')
f0.write("%4d  %4d  %0.3f  %3d  %0.3f\n"%(runNumber,HV, BField, Amp,  meanW4rms))
f0.close()

wfms = [wavesW0, wavesW1, wavesW2,  wavesW3,  wavesW4,  wavesW5,  wavesW6, wavesW7, wavesW8, wavesTr0, wavesTr1]
print("length of wavesW0 is %d"%len(wavesW0))
print("length of 1st waveform in wavesW0 is %d"%len(wavesW0[0]))
indxTr0=9
indxTr1=10
ir0=420; ir1=620	# region of signal events
ir0=0;   ir1=1000	# region of signal events

dr0=825; dr1=950 
ir0=dr0; ir1=dr1	# region of signal events
irr0=900; irr1=1000	# region of signal in rotated plots

iev=0

waveNumb = 0
nev = 10
voffset = 20
#voffset = 10
wantRawWfmPlots=True
#wantRawWfmPlots=False
if wantRawWfmPlots :
    print("Plot raw over ir0,ir1 = ",ir0,ir1)
    #def myWavePlot(id,aTitle,times,waves,waveNumb,nev,voffset,pathname=".",is0=0,is1=1000)
    myWavePlot( 0, "raw W0", times, wavesW0, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 1, "raw W1", times, wavesW1, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 2, "raw W2", times, wavesW2, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 3, "raw W3", times, wavesW3, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 4, "raw W4", times, wavesW4, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 5, "raw W5", times, wavesW5, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 6, "raw W6", times, wavesW6, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 7, "raw W7", times, wavesW7, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 8, "raw W8", times, wavesW8, waveNumb,nev,voffset,pathName,is0=ir0,is1=ir1 )
    myWavePlot( 9, "raw TR0", times, wavesTr0, waveNumb,nev,100.0,pathName,is0=ir0-300,is1=ir1-300 )


wavesTr0rot=[]
wavesTr1rot=[]
wavesW0rot=[]
wavesW1rot=[]
wavesW2rot=[]
wavesW3rot=[]
wavesW4rot=[]
wavesW5rot=[]
wavesW6rot=[]
wavesW7rot=[]
wavesW8rot=[]

thresholdTr=-100.0      # mV
thresholdWs=-1.6        # mV
thresholdW4=-2.5        # mV
thresholdWs=-1.5        # mV
#if Amp == 10  : thresholdW4 = -10.0
if Amp == 100 : thresholdW4 = -15.0
if Amp == 100 : thresholdW4 = -6.0
if BField > 12.0 : thresholdW4 = -0.9
peakBin=615             # time bin to align to
peakBin=580             # time bin to align to
print("Using thresholdTr,thresholdWs,thresholdW4,peakBin = ", thresholdTr,thresholdWs,thresholdW4,peakBin)
print("average noiseW4rms = %0.2f cf threshold %0.2f"%(np.mean(noiseW4rms),thresholdW4))

WTr0timebins=[]; WTr0phs=[]
W0timebins=[]; W0phs=[]; W0Aphs=[]
W1timebins=[]; W1phs=[]; W1Aphs=[]
W2timebins=[]; W2phs=[]; W2Aphs=[]
W3timebins=[]; W3phs=[]; W3Aphs=[]
W4timebins=[]; W4phs=[]; W4Aphs=[]
W5timebins=[]; W5phs=[]; W5Aphs=[]
W6timebins=[]; W6phs=[]; W6Aphs=[]
W7timebins=[]; W7phs=[]; W7Aphs=[]
W8timebins=[]; W8phs=[]; W8Aphs=[]

WTr0timebins=[]; WTr0phs=[]
WTr1timebins=[]; WTr1phs=[]
W0peakCnts=[]; W0peakCharges=[]
W1peakCnts=[]; W1peakCharges=[]
W2peakCnts=[]; W2peakCharges=[]
W3peakCnts=[]; W3peakCharges=[]
W4peakCnts=[]; W4peakCharges=[]
W5peakCnts=[]; W5peakCharges=[]
W6peakCnts=[]; W6peakCharges=[]
W7peakCnts=[]; W7peakCharges=[]
W8peakCnts=[]; W8peakCharges=[]


lowBin = 15; hghBin= 26
lowBin = 50; hghBin= 50
lowBin = 10; hghBin= 10
peakLocs=[]
for iev in range(len(wavesTr0)) :
        if iev%100 == 0 : print("Working on event %d" % iev)
        #
        # get information for trigger waveform
        awaveTr0 = wavesTr0[iev]
        peakLoc = -1
        peakLoc,value = atThres(thresholdTr,awaveTr0)
        Tr0peakTimeEst = 0.200*peakLoc
        WTr0timebins.append(Tr0peakTimeEst)
        peakLocs.append(peakLoc)
        binOffset = peakLoc - peakBin
        awaveTr0rot = rotate( awaveTr0, -binOffset )
        wavesTr0rot.append( awaveTr0rot )
        #
        print("peakLoc = ", peakLoc)
        print("iev, peakLoc, Tr0peakTimeEst, binOffset = ", iev,peakLoc,Tr0peakTimeEst)


        awaveW0 = wavesW0[iev]
        awaveW0rot = rotate( awaveW0, -binOffset )
        wavesW0rot.append(awaveW0rot)
        thresIndex,value = atThres(thresholdWs,awaveW0rot)
        #print("thresIndex,value = ", thresIndex,value)
        W0timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW0rot)
        W0ph = awaveW0rot[peakIndex]
        W0phs.append(W0ph)
        if thresIndex != 1023 : 
           W0Aphs.append(W0ph)
           W0timebins.append(W0timeAtThres)
        #print ("W0 peakIndex, W0ph thresholdWs = ", peakIndex, W0ph, thresholdWs )
        #print ("W0Aphs = ", W0Aphs[-1])
        #print ("W0timebins = ", W0timebins[-1])

        awaveW1 = wavesW1[iev]
        awaveW1rot = rotate( awaveW1, -binOffset )
        wavesW1rot.append(awaveW1rot)
        thresIndex,value = atThres(thresholdWs,awaveW1rot)
        W1timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW1rot)
        W1ph = awaveW1rot[peakIndex]
        W1phs.append(W1ph)
        if thresIndex != 1023 :
           W1Aphs.append(W1ph)
           W1timebins.append(W1timeAtThres)

        awaveW2 = wavesW2[iev]
        awaveW2rot = rotate( awaveW2, -binOffset )
        wavesW2rot.append(awaveW2rot)
        thresIndex,value = atThres(thresholdWs,awaveW2rot)
        W2timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW2rot)
        W2ph = awaveW2rot[peakIndex]
        W2phs.append(W2ph)
        if thresIndex != 1023 :
           W2Aphs.append(W2ph)
           W2timebins.append(W2timeAtThres)


        awaveW3 = wavesW3[iev]
        awaveW3rot = rotate( awaveW3, -binOffset )
        wavesW3rot.append(awaveW3rot)
        thresIndex,value = atThres(thresholdWs,awaveW3rot)
        W3timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW3rot)
        W3ph = awaveW3rot[peakIndex]
        W3phs.append(W3ph)
        if thresIndex != 1023 :
           W3Aphs.append(W3ph)
           W3timebins.append(W3timeAtThres)

        awaveW4 = wavesW4[iev]
        awaveW4rot = rotate( awaveW4, -binOffset )
        wavesW4rot.append(awaveW4rot)
        thresIndex,value = atThres(thresholdW4,awaveW4rot)
        print("thresIndex,value = ", thresIndex,value)
        W4timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW4rot)
        W4minIndex = peakIndex
        W4ph = awaveW4rot[peakIndex]
        W4min = W4ph
        #W4phs.append(-W4ph)
        W4phs.append(W4ph)
        if thresIndex != 1023 : 
           #W4Aphs.append(-W4ph)
           W4Aphs.append(W4ph)
           W4timebins.append(W4timeAtThres)
        #print ("W4 peakIndex, W4ph thresholdWs = ", peakIndex, W4ph, thresholdW4 )
        #print ("W4Aphs = ", W4Aphs[-1])
        #print ("W4timebins = ", W4timebins[-1])


        awaveW5 = wavesW5[iev]
        awaveW5rot = rotate( awaveW5, -binOffset )
        wavesW5rot.append(awaveW5rot)
        thresIndex,value = atThres(thresholdWs,awaveW5rot)
        W5timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW5rot)
        W5ph = awaveW5rot[peakIndex]
        W5phs.append(W5ph)
        if thresIndex != 1023 :
           W5Aphs.append(W5ph)
           W5timebins.append(W5timeAtThres)

        awaveW6 = wavesW6[iev]
        awaveW6rot = rotate( awaveW6, -binOffset )
        wavesW6rot.append(awaveW6rot)
        thresIndex,value = atThres(thresholdWs,awaveW6rot)
        W6timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW6rot)
        W6ph = awaveW6rot[peakIndex]
        W6phs.append(W6ph)
        if thresIndex != 1023 :
           W6Aphs.append(W6ph)
           W6timebins.append(W6timeAtThres)

        awaveW7 = wavesW7[iev]
        awaveW7rot = rotate( awaveW7, -binOffset )
        wavesW7rot.append(awaveW7rot)
        thresIndex,value = atThres(thresholdWs,awaveW7rot)
        W7timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW7rot)
        W7ph = awaveW7rot[peakIndex]
        W7phs.append(W7ph)
        if thresIndex != 1023 :
           W7Aphs.append(W7ph)
           W7timebins.append(W7timeAtThres)

        awaveW8 = wavesW8[iev]
        awaveW8rot = rotate( awaveW8, -binOffset )
        wavesW8rot.append(awaveW8rot)
        thresIndex,value = atThres(thresholdWs,awaveW8rot)
        W8timeAtThres = 0.200*thresIndex
        peakIndex = np.argmin(awaveW8rot)
        W8ph = awaveW8rot[peakIndex]
        W8phs.append(W8ph)
        if thresIndex != 1023 :
           W8Aphs.append(W8ph)
           W8timebins.append(W8timeAtThres)


        if True :	# exact charge for W4
           nciev=10
           if iev<nciev :
              sys.stderr.write("Calc W4 charge\n")
              sys.stderr.write("at %d W4min=%fW4minIndex=%d\n"%(iev,W4min,W4minIndex))
           W4peakCharge=0
           ipeak=0
           ipeakG80 =  0
           iplow = max(0, int(W4minIndex-lowBin))
           iphgh = min(int(W4minIndex+hghBin), 1023)
           if iev<nciev : sys.stderr.write("iplow, iphgh = %d %d\n"%(iplow,iphgh))
           if iev<nciev : sys.stderr.write("iplow, iphgh = %d %d (nsec)\n"%(0.2*iplow,
                                           0.2*iphgh))
           for ip in range(iplow, iphgh ) :
              W4peakCharge += -0.2 * awaveW4rot[ip] / 50     # pCoul
              #if iev<nciev:sys.stderr.write("iev,ip,W4peakCharge = %d %d %0.2f\n"%(iev,
              #                              ip,W4peakCharge))
           if W4min < thresholdW4 : W4peakCharges.append(W4peakCharge)
           if iev< nciev : sys.stderr.write("W4peakCharge = %d\n"%W4peakCharge)

#sys.exit()
sys.stderr.write("Done processing event loop\n")

# end of event loop

sys.stderr.write("lengths of contrib to X,Y %d %d %d %d %d %d\n"%(len(W0phs),len(W3phs),len(W6phs),
len(W1phs),len(W4phs),len(W7phs)))
if False :
  if len(W1phs)>0 and len(W3phs)>0 and len(W4phs)>0 and len(W5phs)>0 and len(W7phs)>0 :
   wX  = np.mean(W1phs) * centers['W1'][0] + np.mean(W4phs) * centers['W4'][0] + np.mean(W7phs) * centers['W2'][0]
   wX = wX/( np.mean(W1phs) + np.mean(W4phs) + np.mean(W7phs)  )
   wY = np.mean(W3phs) * centers['W3'][1] + np.mean(W4phs) * centers['W4'][1] + np.mean(W5phs) * centers['W5'][1]
   wY = wY/( np.mean(W3phs) + np.mean(W4phs) + np.mean(W5phs) )
  else :
   wX=-100.0
   wY=-100.0
if True :
  if len(W1Aphs)>0 and len(W3Aphs)>0 and len(W4Aphs)>0 and len(W5Aphs)>0 and len(W7Aphs)>0 :
   wX = np.mean(W1Aphs) * centers['W1'][0] + np.mean(W4Aphs) * centers['W4'][0] + np.mean(W7Aphs) * centers['W2'][0]
   wX = wX/( np.mean(W1Aphs) + np.mean(W4Aphs) + np.mean(W7Aphs)  )
   wY = np.mean(W3Aphs) * centers['W3'][1] + np.mean(W4Aphs) * centers['W4'][1] + np.mean(W5Aphs) * centers['W5'][1]
   wY = wY/( np.mean(W3Aphs) + np.mean(W4Aphs) + np.mean(W5Aphs) )
  else :
   wX=-100.0
   wY=-100.0
print('ENM X = ',wX, 'Y = ', wY)
sys.stderr.write("Done calculating X,Y\n")

def calcMyMean(aWphs) :
    if len(aWphs)==0 : return np.nan
    else : return np.mean(aWphs)

meanW0Aphs=calcMyMean(W0Aphs)
meanW1Aphs=calcMyMean(W1Aphs)
meanW2Aphs=calcMyMean(W2Aphs)
meanW3Aphs=calcMyMean(W3Aphs)
#sys.stderr.write("Check on W4 mean length of WAphs is %d\n"%len(W4Aphs))
meanW4Aphs=calcMyMean(W4Aphs)
#sys.stderr.write("After Check meanWAphs = %f\n"%meanW4Aphs)
meanW5Aphs=calcMyMean(W5Aphs)
meanW6Aphs=calcMyMean(W6Aphs)
meanW7Aphs=calcMyMean(W7Aphs)
meanW8Aphs=calcMyMean(W8Aphs)

f1=open("binaryReader.meanPHs.dat","a")
#f1.write(sformat%(
#runNumber, numbEvents, HV, BField, Amp, Mode, wX, wY, meanW0Aphs, meanW1Aphs, meanW2Aphs, 
#meanW3Aphs,meanW4Aphs,meanW5Aphs,meanW6Aphs,meanW7Aphs,meanW8Aphs, np.mean(W4peakCharges)))

f1.write(s2format.format( runNumber, numbEvents, HV, BField, Amp, Mode, wX, wY, meanW0Aphs ,meanW1Aphs,meanW2Aphs,meanW3Aphs,meanW4Aphs,meanW5Aphs,meanW6Aphs,meanW7Aphs,meanW8Aphs, np.mean(W4peakCharges)))
f1.close()

sys.stderr.write("Done writing f1 summary data.\n")

wantrotWaveformPlots=False
wantrotWaveformPlots=True 
if wantrotWaveformPlots :
   print("Saving rotated plots with waveNumb, nev, voffset = ",waveNumb, nev, voffset)
   print('Using pathName =',pathName)
   myWavePlot(10, pathName+' waves W0 rotated', times, wavesW0rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(11, pathName+' waves W1 rotated', times, wavesW1rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(-12, pathName+' waves W2 rotated', times, wavesW2rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(-13, pathName+' waves W3 rotated', times, wavesW3rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(14, pathName+' waves W4 rotated', times, wavesW4rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(-15, pathName+' waves W5 rotated', times, wavesW5rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(-16, pathName+' waves W6 rotated', times, wavesW6rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(-17, pathName+' waves W7 rotated', times, wavesW7rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(-18, pathName+' waves W8 rotated', times, wavesW8rot, waveNumb, nev, voffset, pathName,is0=irr0,is1=irr1)
   myWavePlot(-19, pathName+' waves Tr0 rotated', times, wavesTr0rot, waveNumb, nev, 100.0, pathName,is0=irr0-350,is1=irr1-350)


"""start
useLogscale=False
#useLogscale=True
print("101 Histogram number of negative peaks found by peakdet()")
plt.figure(101)
h1=np.histogram(W4peakCnts, bins=10)
values = h1[0]
centers=bincenters(h1[1])
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
plt.title(pathName+" W4 number of peaks"  )
plt.xlabel('number of peaks')
meanNumPksW4=np.mean(W4peakCnts)
plt.savefig(pathName+"/101-W4-histo-numberOfNegPeaks.png")
  
sys.stderr.write("Done with histogram 101.\n")
"""

useLogscale=False
print("\n\n102 Histogram W4 time from peakdet")
debug102=True
plt.figure(102)
#numberOfBins = 200
#h2=np.histogram(W4timebins, bins=numberOfBins)
numberOfBins = 50
bins=np.linspace(180,190,numberOfBins)
h2=np.histogram(W4timebins, bins)
values = h2[0]
centers=bincenters(h2[1])
#if debug102 : print("bin width = ", (centers[numberOfBins-1] - centers[0])/numberOfBins)
delta = (centers[1]-centers[0])
if debug102 : print('delta time bin (ns)is ', delta)
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
(W4lenght, W4min, W4max, W4mean, W4std, methodUsed)=getStats( 'Time W4', W4timebins, 180.0, 190.0 )
plt.title(pathName+" W4 time mean=%0.3f  est rms=%0.3f (ns)"%(W4mean,W4std))
plt.xlabel('Time (ns)')
peakThres = 2.0
maxtabW4time, mintabW4time = peakdet(values,peakThres)
numbPosPeaks = len(maxtabW4time) 
if debug102 :
    for i in range(numbPosPeaks) :
        print("102H:", i,maxtabW4time[i][0], maxtabW4time[i][0]*delta, "(ns)",maxtabW4time[i][1])
if len(maxtabW4time) != 0 :
        peakLoc = maxtabW4time[0][0]
        peakHeight = int(maxtabW4time[0][1])
else :
        peakLoc = 0
        peakHeight = 0
#print("W4 time: numbPosPeaks, peakLoc, peakHeight = ",numbPosPeaks, peakLoc, peakHeight)
getStats( 'Time W4', W4timebins, 50.0, 200.0 )
getStats( 'Time W4', W4timebins, 180.0, 200.0 )
plt.savefig(pathName+"/102-histo-W4-time-from-peakdet.png")

sys.stderr.write("Done with histogram 102.\n")

""" start
plt.figure(103)
bins=np.linspace(-15,+15,150)
h3=np.histogram(W0mW2timebins, bins)
values = h3[0]
centers=bincenters(h3[1])
print 'delta time bin (ns)is ', (centers[1]-centers[0])*0.200
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
(W0mW2lenght, W0mW2min, W0mW2max, W0mW2mean, W0mW2std,methodUsed)=getStats(
 'Time W0-W2', W0mW2timebins, 50.0, 150.0 )
plt.title(pathName+" W0-W2 time  est rms=%0.3f (ns)"%W0std)
plt.xlabel('Time (ns)')
plt.savefig(pathName+"/103-histo-W0-W2-time-deference-from-peakdet.png")
"""
getStats( 'Time W6', W6timebins, 50.0, 150.0 )

print("\n\nHistogram W4 pulse heights")
plt.figure(103)
h4=np.histogram(W4phs, bins=100)
values = h4[0]
centers=bincenters(h4[1])
print('delta ph bin is ', centers[1]-centers[0])
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
#if useLogscale : plt.semilogy(centers, values )
plt.semilogy(centers, values )
getStats( 'ph W4', W4phs, 0.0, 200.0 )
meanPhW4 = np.mean(W4phs)
npeW4 = ( np.mean(W4phs)/(np.std(W4phs)+0.000001) )**2
getStats( 'ph W4', W4phs, 0.0, 200.0 )
getStats( 'ph W4 with lower bound', W4Aphs, -thresholdW4, 200.0 )
plt.title(pathName+" W4 pulse height meanPH=%0.f est npe=%0.3f"%(meanPhW4,npeW4))
plt.xlabel('pulse height (mV)')
plt.savefig(pathName+"/103-histo-W4-pulse-height.png")
(W4No0pHlength,W4No0pHmin,W4No0pHmax,W4No0phmean,W4No0phstd,methodUsed)=getStats( 
'W4 No0 Ph', W4phs, 0.1, 200.0, 1, 1 )
if W4No0pHlength != 0 :
   print("W4ph (no 0s) count mean, std, method  = ", W4No0pHlength,W4No0phmean, W4No0phstd, methodUsed)


print("\n\nHistogram W4 pulse heights")
plt.figure(104)
#h4=np.histogram(W4phs, bins=100)
h4=np.histogram(W4Aphs, bins=100)
values = h4[0]
centers=bincenters(h4[1])
print('delta ph bin is ', centers[1]-centers[0])
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
getStats( 'ph W4', W4phs, 0.0, 200.0 )
meanPhW4A = np.mean(W4Aphs)
npeW4A = ( np.mean(W4Aphs)/(np.std(W4Aphs)+0.000001) )**2
getStats( 'ph W4A', W4Aphs, 0.0, 200.0 )
getStats( 'ph W4A with lower bound', W4Aphs, -thresholdW4, 200.0 )
plt.title(pathName+" W4A pulse height mean %0.3f est npe=%0.3f"%(meanPhW4A,npeW4A))
plt.xlabel('pulse height (mV)')
plt.savefig(pathName+"/104-histo-W4-pulse-height.png")
(W4No0pHlength,W4No0pHmin,W4No0pHmax,W4No0phmean,W4No0phstd,methodUsed)=getStats( 
'W4 No0 Ph', W4phs, -thresholdW4, 200.0, 1, 1 )
if W4No0pHlength != 0 :
   print("W4ph (no 0s) count mean, std, method  = ", W4No0pHlength,W4No0phmean, W4No0phstd, methodUsed)



getStats( 'ph W0', W0phs, 0.0, 200.0 )
if len(W0phs) == 0 : npeW0 = np.nan
else : npeW0 = ( np.mean(W0phs)/(np.std(W0phs)+0.000001) )**2

getStats( 'ph W1', W1phs, 0.0, 200.0 )
if len(W1phs) == 0 : npeW1 = np.nan
else : npeW1 = ( np.mean(W1phs)/(np.std(W1phs)+0.000001) )**2

getStats( 'ph W2', W2phs, 0.0, 200.0 )
if len(W2phs) == 0 : npeW2 = np.nan
else : npeW2 = ( np.mean(W2phs)/(np.std(W2phs)+0.000001) )**2

getStats( 'ph W3', W3phs, 0.0, 200.0 )
if len(W3phs) == 0 : npeW3 = np.nan
else : npeW3 = ( np.mean(W3phs)/(np.std(W3phs)+0.000001) )**2

getStats( 'ph W4', W4phs, 0.0, 200.0 )
if len(W4phs) == 0 : npeW4 = np.nan
else : npeW4 = ( np.mean(W4phs)/(np.std(W4phs)+0.000001) )**2

getStats( 'ph W5', W5phs, 0.0, 200.0 )
if len(W5phs) == 0 : npeW5 = np.nan
else : npeW5 = ( np.mean(W5phs)/(np.std(W5phs)+0.000001) )**2

getStats( 'ph W6', W6phs, 0.0, 200.0 )
if len(W6phs) == 0 : npeW6 = np.nan
else : npeW6 = ( np.mean(W6phs)/(np.std(W6phs)+0.000001) )**2

getStats( 'ph W7', W7phs, 0.0, 200.0 )
if len(W7phs) == 0 : npeW7 = np.nan
else : npeW7 = ( np.mean(W7phs)/(np.std(W7phs)+0.000001) )**2

getStats( 'ph W8', W8phs, 0.0, 200.0 )
if len(W8phs) == 0 : npeW8 = np.nan
else : npeW8 = ( np.mean(W8phs)/(np.std(W8phs)+0.000001) )**2

print("ENM  npeW0 npeW1 npeW2 npeW3 npeW4 npeW5 npeW6 npeW7 npeW8")
print("ENM  %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f   %0.2f"%(
 npeW0, npeW1, npeW2, npeW3, npeW4, npeW5, npeW6,npeW7, npeW8))

f2=open("binaryReader.NPEs.dat","a")
"""start
f2.write(sformat%( runNumber,numbEvents, HV, BField, Amp, Mode, wX, wY, 
npeW0, npeW1, npeW2, npeW3, npeW4, npeW5, npeW6,npeW7, npeW8 , np.mean(W4peakCharges)
)) 
"""
f2.write(s2format.format(runNumber, numbEvents, HV, BField, Amp, Mode, wX, wY, npeW0, npeW1, npeW2, npeW3, npeW4, npeW5, npeW6,npeW7, npeW8 , np.mean(W4peakCharges)))
f2.close()

plt.figure(105)
h5=np.histogram(W4peakCharges, bins=50)
values = h5[0]
centers=bincenters(h5[1])
errs=generateerrorbars(values)
plt.errorbar(centers, values, yerr=errs, fmt='o')
if useLogscale : plt.semilogy(centers, values )
plt.title(pathName+" W4 peak total Charge "  )
plt.xlabel('Charge (pCoul)')
meanChargeW4=np.mean(W4peakCharges)
stdChargeW4=np.std(W4peakCharges)+0.0000001      #enm force non-zero
npeChargeW4= (meanChargeW4/stdChargeW4)**2
plt.title(pathName+" W4 peak total meanCharge = %f  npe = %f " % (meanChargeW4,npeChargeW4  ))
plt.savefig(pathName+"/105-histo-W4-peak-total-charge.png")
print("meanChargeW4, npeChargeW4 = ",meanChargeW4, npeChargeW4)


print('pathName = ', pathName)


sys.stderr.write("Done\n")
print("Stopping early"); exit()

#======================================================================================

#plt.show()

t1=time.time()
print("Job time %0.2f (sec)"%(t1-t0))
sys.exit()
        
print('linecount = ',  linecount)


wavesW0rot=[]
wavesTrrot=[]
for i in range(len(waves)) :
        awaveW0 = waves[i]
        offset = TrBinOffsets[i]
        awaveW0rot = rotate( awaveW0, -offset )
        wavesW0rot.append( awaveW0rot )
        awaveTr = wavesTr[i]
        awaveTrrot = rotate( awaveTr, -offset )
        wavesTrrot.append( awaveTrrot )

from operator import add
#map(add, list1, list2)

#sumW0 = [0]*1024
#print 'length of sumW0 = ', len(sumW0)
#for i in range(len(wavesW0rot)) :
#       sumW0 = map(add, sumW0, wavesW0rot[i] )

# collect data into 3 groups of 30 triggers
sumW0s = []
aveW0s = []
for grp in range(0,3) :
   sumW0 = [0]*1024
   for i in range( 30*grp, 30*(grp+1) ) :
        sumW0 = list(map(add, sumW0, wavesW0rot[i] ))
   sumW0s.append(sumW0)
   aveW0 = newList = [x / 30 for x in sumW0]
   aveW0s.append(aveW0)



waveNumb = 25
voffset = 10.0 # mV
voffset = 100.0 # mV
nev = 25
print("in main: length of times is ", len(times)," length of waves is ", len(waves))
voffset = 10.0 # mV
#voffset = 50.0 # mV
#myWavePlot( 100, times, waves, waveNumb, nev, voffset )
myWavePlot( 100, times, wavesW0rot, waveNumb, nev, voffset )

voffset = 100.0 # mV
#myWavePlot( 101, times, wavesTr, waveNumb, nev, voffset )
myWavePlot( 101, times, wavesTrrot, waveNumb, nev, voffset )

voffset=10.
#voffset=100.
myWavePlot( 102, times, sumW0s , 0, 3, 30*voffset  )
myWavePlot( 103, times, aveW0s , 0, 3, voffset  )

plt.show()

sys.exit()

text_file = open("binaryReader.dat", "r")
#lines = text_file.readlines()
lines = text_file.read().split(' ')
print(lines[0])
print('length of lines is ',len(lines))
print('length of line is ',len(lines[0]))
print('lines[-1] = ', lines[-1], "\n = lines[-1]")
wave=[]
waves=[]
for i in range(len(lines)) :
        wave=[]
        aLine=lines[i]
        print('length of aLine = ',len(aLine))
        for j in range(len(aLine)) :
                print(j, aLine[j])
                wave.append( float(aLine[j] ) )
        waves.append(wave)
print(wave)
print('length of wave is ', len(wave))
print('length of waves is ', len(waves))
text_file.close()

sys.exit()

with open('special.dat') as f:
    for line in f:
        #inner_list = [elt.strip() for elt in line.split(' ')]
        # in alternative, if you need to use the file content as numbers
        inner_list = [int(elt.strip()) for elt in line.split(' ')]
        list_of_lists.append(inner_list)
print('length of list_of_lists is ', len(list_of_lists ))

