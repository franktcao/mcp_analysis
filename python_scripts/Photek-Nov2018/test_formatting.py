

myInts=[ 1,10,100,-1,-100 ]
myFloats=[ 1.2,10.31,100.21, -100.41, -1000.61]
f0=open("zformat.dat","w")
print(len(myInts))
for i in range(len(myInts)) :
	print('{0:3d} {1:7.2f} '.format(myInts[i], myFloats[i]))
	f0.write('{0:3d} {1:7.2f} \n'.format(myInts[i], myFloats[i]))

f0.close()
