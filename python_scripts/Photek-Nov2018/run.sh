#!/bin/bash

export XDG_RUNTIME_DIR='/tmp/runtime-may'

#Usage: binaryReaderV7.py  runNumber numbEvts HV BField Amp Mode

/bin/rm -f binaryReader.NPEs.dat  binaryReader.meanPHs.dat noise.dat
echo Place headers
time python binaryReaderV7.py 0      1000    4000 0.13  1  SPE

time python binaryReaderV7.py 1006   1000    4000 0.13  1  MPE	# may have wrong parameters

#time python binaryReaderV7.py 2008   1000    4000 0.13  1  SPE
#time python binaryReaderV7.py 2006   1000    4000 0.13  10 SPE

#time python binaryReaderV7.py 2139   1000    4000 14.45 1  SPE
#time python binaryReaderV7.py 2130   1000    4000 14.45 10  SPE
#time python binaryReaderV7.py 2137   1000    4000 14.45 100  SPE

echo "$0 is done."  >/dev/stderr
